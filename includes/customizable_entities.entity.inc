<?php
/**
 *  A controller implementing exportables stored in the database.
 *
 * Extend the common class 'EntityAPIControllerExportable' for entities.
 */
class CustomizableEntitiesEntity extends Entity {
  /**
   * Returns the uri of the entity just as entity_uri().
   *
   * Modules may alter the uri by specifying another 'uri callback' using
   * hook_entity_info_alter(). Set uri from EsanInfos::crud path value.
   *
   * @see entity_uri()
   */
  protected function defaultUri() {
    $id = $this->identifier();
    if (!is_numeric($id)) {
      $id = str_replace('_', '-', $id);
    }
    return array(
      'path' => $this->entityInfo['customizable entities']['crud path'] . "/{$id}",
    );
  }


}
