<?php
/**
 * Class managing entities.
 *
 * Extend the common class 'Entity' for entities.
 */
class CustomizableEntitiesEntityController extends EntityAPIControllerExportable {

  /**
   * Provide default values on creating entity.
   *
   * @return
   *   An array, the defaut values keyed by there field name.
   */
  public function entity_default_values($values = array()) {

    $custom_fields = $this->entity_default_values_custom($values);

    $default_values = array();
    // set primary field
    if (!empty($this->primary_key)) {
      $default_values[$this->primary_key] = NULL;
    }
    // set date fields.
    if (in_array('created', $this->entityInfo['schema_fields_sql']['base table'])) {
      // 'created' and 'changed' fields are handled into the save() method.
      $default_values['created'] = NULL;
    }
    if (in_array('changed', $this->entityInfo['schema_fields_sql']['base table'])) {
      // 'created' and 'changed' fields are handled into the save() method.
      $default_values['changed'] = NULL;
    }

    // Set user field
    if (in_array('uid', $this->entityInfo['schema_fields_sql']['base table'])) {
      // 'user' field is handled into the save() method.
      // The connected user is set in customizable_entities_entity_prepare().
      $default_values['uid'] = 0;
    }

    if (in_array('language', $this->entityInfo['schema_fields_sql']['base table'])) {
      // 'language' field is handled into the save() method.
      global $language;
      $default_values['language'] = $language->language;
    }
    // set the vid field.
    if (!empty($this->entityInfo['entity keys']['revision'])) {
      // 'vid' field is handled into the save() method.
      $default_values[$this->entityInfo['entity keys']['revision']] = NULL;
    }
    if (!empty($this->entityInfo['entity keys']['label'])) {
      $default_values[$this->entityInfo['entity keys']['label']] = '';
    }
    if (!empty($this->entityInfo['entity keys']['bundle'])) {
      $default_values[$this->entityInfo['entity keys']['bundle']] = '';
    }
    if (!empty($this->entityInfo['entity keys']['name'])) {
      $default_values[$this->entityInfo['entity keys']['name']] = '';
    }
    if (in_array('weight', $this->entityInfo['schema_fields_sql']['base table'])) {
      $default_values['weight'] = 0;
    }
    if (in_array('description', $this->entityInfo['schema_fields_sql']['base table'])) {
      $default_values['description'] = '';
    }
    if (in_array('help', $this->entityInfo['schema_fields_sql']['base table'])) {
      $default_values['help'] = '';
    }
    if (in_array('title_label', $this->entityInfo['schema_fields_sql']['base table'])) {
      $default_values['title_label'] = 'title';
    }
    if (in_array('data', $this->entityInfo['schema_fields_sql']['base table'])) {
      $default_values['data'] = array();
    }
    // Do not provide default values for status, revison, sticky or promote :
    // they depend on the bundle. I set them into the save() fonction if not set.

    return array_merge($default_values, $custom_fields);

  }

  /**
   * Provide default values on entity custom fields.
   *
   * Each class should implement it.
   *
   * @return
   *   An array, the defaut values keyed by there field name.
   */
  protected function entity_default_values_custom($values = array()) {
    return array();
  }


  /**
   * Overridden.
   *
   * Set default values on creating entity.
   */
  public function create(array $values = array()) {
    // Add values that are specific to our customizable_entities
    // @TODO : set default values.
    $fields = $this->entity_default_values($values);
    if (!empty($fields)) {
      $values += $fields;
    }
    $entity = parent::create($values);
    return $entity;
  }

  /**
   * Overridden.
   *
   * Add The entity_type name in the entity. issue https://www.drupal.org/node/1042822
   */
  public function load($ids = array(), $conditions = array()) {


    // https://www.drupal.org/node/1042822#comment-6688828
    $entities = parent::load($ids, $conditions);
    $uids = array();
    foreach ($entities as $id => $entity) {
      $entity->entity_type = $this->entityType;
      // fork of user_node_load() :
      if (in_array('uid', $this->entityInfo['schema_fields_sql']['base table'])) {
        $uids[$id] = $entity->uid;
      }
    }

    if (in_array('uid', $this->entityInfo['schema_fields_sql']['base table']) && !empty($uids)) {
      // Fetch name, picture, and data for these users.
      $user_fields = db_query("SELECT uid, name, picture, data FROM {users} WHERE uid IN (:uids)", array(':uids' => $uids))->fetchAllAssoc('uid');

      // Add these values back into the node objects.
      foreach ($uids as $id => $uid) {
        // set it as USERname instaed of name in order to avoid name conflict
        $entities[$id]->username = $user_fields[$uid]->name;
        $entities[$id]->picture = $user_fields[$uid]->picture;
        // set it as dataS instead of data in order to avoid name conflict
        $entities[$id]->datas = $user_fields[$uid]->data;
      }
    }
    return $entities;
  }


  public function save($entity, DatabaseTransaction $transaction = NULL) {

    // save info into fixed variable for later use.
    $insert = !empty($entity->is_new);

    // Load the stored entity, if any.
    if (!empty($entity->{$this->idKey}) && !isset($entity->original)) {
      // In order to properly work in case of name changes, load the original
      // entity using the id key if it is available.
      $entity_original = entity_load_unchanged($this->entityType, $entity->{$this->idKey});
      // Do not load twice:
      $entity->original = $entity_original;
    }

    // Flag indication this entity was generated from user interface.
    $display_message = (!empty($entity->from_ui));

    if (in_array('created', $this->entityInfo['schema_fields_sql']['base table'])) {
      // Care about setting created and changed values. But do not automatically
      // set a created values for already existing entity.
      if (empty($entity->created) && !empty($entity->is_new)) {
        $entity->created = REQUEST_TIME;
      }
    }
    if (in_array('changed', $this->entityInfo['schema_fields_sql']['base table'])) {
      $entity->changed = REQUEST_TIME;
    }
    if (in_array('export_status', $this->entityInfo['schema_fields_sql']['base table']) && !empty($entity->is_new)) {
      $entity->export_status = ENTITY_CUSTOM;
      $entity->export_module = $this->entityInfo['module'];
    }

    if (!empty($this->entityInfo['entity keys']['revision'])) {
      if (in_array('timestamp', $this->entityInfo['schema_fields_sql']['revision table'])) {
        $entity->timestamp = REQUEST_TIME;
      }

      if (in_array('log', $this->entityInfo['schema_fields_sql']['revision table'])) {

        if ((!empty($entity->is_new)) || !empty($entity->revision)) {
          // When inserting either a new entity or a new node revision, $entity->log
          // must be set because {revision}.log is a text column and therefore
          // cannot have a default value. However, it might not be set at this
          // point (for example, if the user submitting a entity form does not have
          // permission to create revisions), so we ensure that it is at least an
          // empty string in that case.
          // @todo: Make the {revision}.log column nullable so that we can
          // remove this check.
          if (!isset($entity->log)) {
            $entity->log = '';
          }
        }
        elseif (!isset($entity->log) || $entity->log === '') {
          // If we are updating an existing entity without adding a new revision, we
          // need to make sure $entity->log is unset whenever it is empty. As long as
          // $entity->log is unset, drupal_write_record() will not attempt to update
          // the existing database column when re-saving the revision; therefore,
          // this code allows us to avoid clobbering an existing log entry with an
          // empty one.
          unset($entity->log);
        }
      }
    }

    if (in_array('uid', $this->entityInfo['schema_fields_sql']['base table'])) {
      global $user;
      if (!isset($entity->uid) && !empty($entity->is_new)) {
        $entity->uid = 0;
      }
      $entity->revision_uid = $user->uid;
    }

    if (in_array('language', $this->entityInfo['schema_fields_sql']['base table'])) {
      if (empty($entity->language) && !empty($entity->is_new)) {
        global $language;
        $entity->language = $language->language;
      }
    }
    // Use entity_revision_set_default($entity_type, $entity) for make-current.
    // Set default values for promote, sticky, revision,
    $variable_options = customizable_entities_variable_datas($this->entityType, $entity, 'options', 'get');
    if (!isset($entity->{$this->entityInfo['entity keys']['id']}) || isset($entity->is_new)) {
      foreach (array('status', 'promote', 'sticky') as $key) {
        // Multistep node forms might have filled in something already.
        if (!isset($entity->$key) && in_array($key, $this->entityInfo['schema_fields_sql']['base table'])) {
          $entity->$key = (int) in_array($key, $variable_options);
        }
      }
    }
    if (!empty($this->entityInfo['entity keys']['revision']) && !isset($entity->revision)) {
      $entity->revision = in_array('revision', $variable_options);
    }

    $return = parent::save($entity, $transaction);

    // Watchdog.
    // Handle bundle if any.
    $human_bundle_label = '';
    if (!empty($this->entityInfo['entity keys']['bundle']) && !empty($this->entityInfo['bundles'])) {
      // The entity has bundles.
      if ($entity) {
        $type = $entity->{$this->entityInfo['entity keys']['bundle']};
        if ($bundle_entity_name = customizable_entities_entity_find_bundle_entity_type($this->entityType)) {
          $bundle = $entity->{$this->entityInfo['entity keys']['bundle']};
          $human_bundle_label = entity_label($bundle_entity_name, $bundle);
        }
      }
    }

    $name = (!empty($human_bundle_label)) ? $human_bundle_label . $this->entityInfo['label'] : $this->entityInfo['label'];
    $watchdog_args = array(
      '@type' => drupal_strtoupper(drupal_substr($name, 0, 1)) . drupal_substr($name, 1),
      '%title' => entity_label($this->entityType, $entity),
    );
    $link = l(t('view'), current(entity_uri($this->entityType, $entity)));
    if (in_array($return, array(SAVED_NEW, SAVED_UPDATED))) {
      $severity = WATCHDOG_NOTICE;
      $message = ($insert) ? '@type: added %title.' : '@type: updated %title.';
    }
    else {
      $severity = WATCHDOG_ERROR;
      $message = '@type: saving process has failed for %title';
    }
    watchdog($this->entityInfo['module'], $message, $watchdog_args, $severity, $link);

    if ($display_message) {
      if ($insert) {
        drupal_set_message(t('@type %title has been created.', $watchdog_args));
      }
      else {
        drupal_set_message(t('@type %title has been updated.', $watchdog_args));
      }
    }

    // manage bundles
    if (isset($this->entityInfo['bundle of']) && $sub_entity_type = $this->entityInfo['bundle of']) {
      $bundle_value = (!empty($entity->{$this->bundleKey})) ? $entity->{$this->bundleKey} : FALSE;
      $old_bundle_value = NULL;
      $sub_entity_info = entity_get_info($sub_entity_type);
      if ($return == SAVED_UPDATED && !empty($this->bundleKey) && !empty($entity_original->{$this->bundleKey}) && $entity_original->{$this->bundleKey} != $entity->{$this->bundleKey}) {
        // Bundle name has changed.
        // field_attach_rename_bundle() is called, managing fields.
        // Manage bundles entities changing the bundle value :
        $old_bundle_value = $entity_original->{$this->bundleKey};
        $new_bundle_value = (!empty($entity->{$this->bundleKey})) ? $entity->{$this->bundleKey} : FALSE;
        // retreive the bundled entity table.
        $sub_entity_table = ($sub_entity_info && !empty($sub_entity_info['base table'])) ? $sub_entity_info['base table'] : FALSE;
        $sub_entity_bundle_col = (!empty($sub_entity_info['entity keys']['bundle'])) ? $sub_entity_info['entity keys']['bundle'] : FALSE;
        if ($sub_entity_table && $sub_entity_bundle_col && $new_bundle_value) {
          $update_count = db_update($sub_entity_table)
            ->fields(array($sub_entity_bundle_col => $new_bundle_value))
            ->condition($sub_entity_bundle_col, $old_bundle_value)
            ->execute();
          if ($update_count) {
            $change_args = array(
              '%old-type' => $old_bundle_value,
              '%type' => $new_bundle_value,
              '@entity_type' => $sub_entity_info['label'],
              '@count' => $update_count,
            );

            $severity = WATCHDOG_NOTICE;
            $link = l(t('edit'), current(entity_uri($this->entityType, $entity)) . '/edit');
            $unformated_message = ($update_count > 1) ? 'Changed the @entity_type type of @count entities from %old-type to %type.' : 'Changed the @entity_type type of 1 entity from %old-type to %type.';
            watchdog($sub_entity_info['module'], $unformated_message, $change_args, $severity, $link);
            if ($display_message) {
              $change_message = format_plural($update_count, 'Changed the @entity_type type of 1 entity from %old-type to %type.', 'Changed the @entity_type type of @count entities from %old-type to %type.', $change_args);
              drupal_set_message($change_message);
            }
          }
        }
      }

      // Save options if have changed.
      if (in_array($return, array(SAVED_NEW, SAVED_UPDATED))) {
        $options_changed = customizable_entities_entity_options_save($sub_entity_type, $entity, $bundle_value, $old_bundle_value);
        if ($options_changed && $display_message) {
          // No need to inform the user.
        }
      }
    }

    // If the entity is new, refresh entity_info in order to the destination
    // entity type to appears in it.
    if ($insert) {
      entity_info_cache_clear();
    }
    // If the saved entity generated more entities or is bundle of another
    // entity : update entity infos and menu.
    if (!empty($this->entityInfo['bundle of']) || customizable_entities_entity_find_destination_entity_types($this->entityType, $entity)) {
      if (!$insert) {
        entity_info_cache_clear();
      }
      entity_property_info_cache_clear();
      field_info_cache_clear();
      //cache_clear_all('field_info:bundle_extra:', 'cache_field', TRUE);
      entity_get_info();
      entity_get_property_info();
      menu_rebuild();
    }
  }

  public function view($entities, $view_mode = 'full', $langcode = NULL, $page = NULL) {
    if (empty($this->entityInfo['fieldable'])) {
      // Prepare extra fields.
      field_attach_prepare_view($this->entityType, $entities, $view_mode);
    }
    $view = parent::view($entities, $view_mode, $langcode, $page);
    // change theme template from entity.tpl.php to customizable_entities.tpl.php
    foreach ($view[$this->entityType] as $key => $entity_build_content) {
      if (!empty($view[$this->entityType][$key]['#theme']) && $view[$this->entityType][$key]['#theme'] == 'entity') {
        $view[$this->entityType][$key]['#theme'] = 'customizable_entities';
      }
      if (empty($this->entityInfo['fieldable'])) {
        // Display extra fields according to the defined weight and visibility.
        $view[$this->entityType][$key]['#pre_render'][] = '_field_extra_fields_pre_render';
        $view[$this->entityType][$key]['#bundle'] = $this->entityType;
      }
    }
    return $view;
  }

  // Delete associated variables
  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    $entities = $ids ? $this->load($ids) : FALSE;
    if (!$entities) {
      // Do nothing, in case invalid or no ids have been passed.
      return;
    }

    $reset_cache = FALSE;
    // Save the entity_info before deletion in this variable.
    $ce_infos = customizable_entities_get_info_customizable_entities();

    parent::delete($ids, $transaction);

    foreach ($entities as $id => $entity) {

      // If the deleted entity generated more entities, delete sub entity
      if ($destination_infos = customizable_entities_entity_find_destination_entity_types($this->entityType, $entity)) {
        foreach ($destination_infos as $entity_type => $info) {
          $reset_cache = TRUE;
          // delete the associated entity.
          customizable_entities_unsinstall_entity_type($entity_type, $ce_infos);
          $label = (!empty($ce_infos[$entity_type]['label'])) ? $ce_infos[$entity_type]['label'] : $entity_type;
          drupal_set_message(t('Deleted Entity type @label.', array('@label' => $label)));
        }
      }

      if (!empty($this->entityInfo['bundle of'])) {
        // This is a Bundle entity, delete associated variables.
        $suffixes = array(
          'options',
          'preview',
          'submitted',
        );

        $bundle = $entity->{$this->bundleKey};
        foreach ($suffixes as $suffix) {
          $variable_name = customizable_entities_variable_datas($this->entityInfo['bundle of'], $bundle, $suffix, 'name');
          variable_del($variable_name);
        }

        // If entity is bundle of another entity than contains datas, delete those datas.
        if ($bundle_datas = customizable_entities_bundle_content_exists($this->entityType, $entity)) {
          $reset_cache = TRUE;
          // delete entities with this bundle.
          $bundle_name = $bundle;
          $sub_entity_info = $bundle_datas['sub_entity_info'];
          $bundle_label = $entity->{$this->entityInfo['entity keys']['label']};
          $delete_count = db_delete($sub_entity_info['base table'])
            ->condition($sub_entity_info['entity keys']['bundle'], $bundle_name)
            ->execute();
          if ($delete_count) {
            $args = array(
              '%type' => $bundle_label,
              '@entity' => $sub_entity_info['label'],
              '@entities' => $sub_entity_info['plural label'],
              '@count' => $delete_count,
            );
            $delete_message = format_plural($delete_count, 'Delete 1 @entity of the %type type.', 'Delete @count @entities of the %type type.', $args);
            drupal_set_message($delete_message);
            $severity = WATCHDOG_NOTICE;
            $link = $sub_entity_info['customizable entities']['overview path'];
            $unformated_message = ($delete_count > 1) ? 'Delete @count @entities of the %type type.' : 'Delete 1 @entity of the %type type.';
            watchdog($this->entityInfo['module'], $unformated_message, $args, $severity, $link);
          }
        }
      }
    }

    if ($reset_cache) {
      entity_info_cache_clear();
      entity_property_info_cache_clear();
      field_info_cache_clear();
      //cache_clear_all('field_info:bundle_extra:', 'cache_field', TRUE);
      entity_get_info();
      entity_get_property_info();
      menu_rebuild();
    }
  }

  protected function renderEntityProperty($wrapper, $name, $property, $view_mode, $langcode, &$content) {

    parent::renderEntityProperty($wrapper, $name, $property, $view_mode, $langcode, $content);

    // See template_preprocess_entity_property()
    // See theme_entity_property();
    // See entity_property_default_render_value_by_type()

    $info = $property->info();
    // Format the date, displaying human date instead of timestamp.
    if (in_array($name, array('created', 'changed')) || (!empty($info['type']) && $info['type'] == 'date')) {
      $ts = $wrapper->$name->value();
      if ($ts) {
        $content[$name]['#content'] = format_date($ts);
      }
    }

    if (!empty($info['type']) && $info['type'] == 'uri') {
      // For uri, display the the label as text of a link.
      $uri = $wrapper->$name->value();
      if ($uri) {
        $label = (!empty($info['label'])) ? $info['label'] : $uri;
        $content[$name]['#content'] = l($label, $uri);
        $content[$name]['#label_hidden'] = TRUE;
      }
    }

    // Do not display label for empty result (as fields).
    if (!$property->label() && $property->type() != 'boolean') {
      $value = check_plain($property->value());
      if ($value === '') {
        unset($content[$name]);
        return;
      }
    }

    // @TODO : display label or not from UI.
    //$content[$name]['#label_hidden'] = TRUE or FALSE, depending on settings.

  }

  /**
   * EntityAPIControllerExportable skip rules (present in EntityAPIController).
   * We add rules events here.
   */
  public function invoke($hook, $entity) {
    parent::invoke($hook, $entity);
    if (module_exists('rules')) {
      rules_invoke_event($this->entityType . '_' . $hook, $entity);
    }
  }
}

