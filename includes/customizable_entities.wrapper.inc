<?php
/**
 * @file
 * Provides basic entity property info for entities provided via the customizable_entities module.
 */

class CustomizableEntitiesMetadataController extends EntityDefaultMetadataController {
  protected $ce_fields = array();

  public function __construct($type) {
    parent::__construct($type);
    // Set the customizable entities fields names.
    $this->ce_fields = cutomizable_entities_defined_properties($this->info, TRUE);
  }

  /**
   * Provide hook_entity_property_info() for the entity.
   *
   * Thoses informations will be used into views, rules, token modules and the
   * entity view page.
   *
   * @return
   *   An array, the entity_property_info() informations.
   *
   *   @see hook_entity_property_info()
   */
  public function entityPropertyInfo() {

    // Retreive all entityinfo setted from schema, for all columns.
    $property_info = parent::entityPropertyInfo();
    // Rewrite dp colomns.
    $this->entityPropertyInfo_dp_fields($property_info);
    // Find custom colomns.
    // split properties between 2 categories.
    $custom_properties = $property_info[$this->type]['properties'];
    $ce_properties = array();
    foreach ($this->ce_fields as $col) {
      if (isset($custom_properties[$col])) {
        $ce_properties[$col] = $custom_properties[$col];
        unset($custom_properties[$col]);
      }
    }
    // Alter or delete custom properties.
    $custom_properties = $this->entityPropertyInfo_custom_fields($custom_properties);

    // add or overwrite to default :
    $property_info[$this->type]['properties'] = array_merge($ce_properties, $custom_properties);
    return $property_info;
  }

  /**
   * Provided entity_property_info() informations for DP fields.
   *
   * DP fields are 'uid', 'created', 'changed', 'vid', etc...
   *
   * @return
   *   An array, the DP entity_property_info() fields.
   *
   * @see CustomizableEntitiesEntityInfo::entity_property_info()
   */
  protected function entityPropertyInfo_dp_fields(&$property_info) {
    // No html for label.

    $info = $property_info[$this->type]['properties'];
    if (empty($info)) {
      $info = array();
    }
    $arg_label = array('@type' => drupal_ucfirst($this->info['label']));
    $arg_desc = array(
      '%type' => drupal_ucfirst($this->info['label']),
      '%types' => drupal_ucfirst($this->info['plural label']),
    );

    $setter_permission = array('setter_permission' => customizable_entities_permission_item('administer', $this->type));

    // colomns handled by parent::entityPropertyInfo() :
    // primary key, type_col_bundle, type_col_entity, label_col, title_col
    // Fix for bundle entity_type permission
    if (!empty($this->info['entity keys']['bundle'])) {
      $info[$this->info['entity keys']['bundle']]['setter_permission'] = $setter_permission['setter_permission'];
    }
    if (!empty($this->info['entity keys']['label'])) {
      $info[$this->info['entity keys']['label']]['required'] = TRUE;
      $info[$this->info['entity keys']['label']]['setter callback'] = 'entity_property_verbatim_set';
    }

    // Rewrite the date colomns.
    if (in_array('created', $this->info['schema_fields_sql']['base table'])) {
      $info['created'] = array(
        'label' => t('Date created'),
        'description' => t('The Unix timestamp when the %type was created.', $arg_desc),
        'type' => 'date',
        'getter callback' => 'entity_metadata_customizable_entities_get_properties',
        'setter callback' => 'entity_property_verbatim_set',
        'schema field' => 'created',
      ) + $setter_permission;
    }
    if (in_array('changed', $this->info['schema_fields_sql']['base table'])) {
      $info['changed'] = array(
        'label' => t('Date changed'),
        'description' => t('The Unix timestamp when the %type was most recently saved.', $arg_desc),
        'type' => 'date',
        'getter callback' => 'entity_metadata_customizable_entities_get_properties',
        'schema field' => 'changed',
      );
    }

    // Rewrite the user colomn.
    if (in_array('uid', $this->info['schema_fields_sql']['base table'])) {
      $info['uid'] = array(
        'label' => t('Author', $arg_label),
        'description' => t('The author of the %type.', $arg_desc),
        'type' => 'user',
        'getter callback' => 'entity_property_verbatim_get',
        'setter callback' => 'entity_property_verbatim_set',
        'required' => TRUE,
        'schema field' => 'uid',
      ) + $setter_permission;
    }

    // Rewrite the language colomn.
    if (in_array('language', $this->info['schema_fields_sql']['base table'])) {
      $info['language'] = array(
        'label' => t('Language'),
        'description' => t('The language the %type is written in.', $arg_desc),
        'type' => 'token',
        'setter callback' => 'entity_property_verbatim_set',
        'options list' => 'entity_metadata_language_list',
        'required' => TRUE,
        'schema field' => 'language',
      ) + $setter_permission;
    }

    // Rewrite the vid colomn.
    $rev_data = '';
    if (!empty($this->info['entity keys']['revision'])) {

      $info['vid'] = array(
        'label' => t('Revision ID'),
        'description' => t('The %type version ID.', $arg_desc),
        'type' => 'integer',
        'validation callback' => 'entity_metadata_validate_integer_positive',
        'schema field' => 'vid',
      );

      $info['log'] = array(
        'label' => t(" Revision log message"),
        'type' => 'text',
        'description' => t("In case a new revision is to be saved, the log entry explaining the changes for this version."),
        'schema field' => 'log',
        'setter callback' => 'entity_property_verbatim_set',
        'access callback' => 'entity_metadata_customizable_entities_revision_access',
      );

      $rev_data = ' (at the time of this revision)';

      // add the revision flag.
      $info['revision'] = array(
        'label' => t('Creates revision'),
        'description' => t('Whether saving this %type creates a new revision.', $arg_desc),
        'type' => 'boolean',
        'setter callback' => 'entity_property_verbatim_set',
        'access callback' => 'entity_metadata_customizable_entities_revision_access',
      );

    }

    // Rewrite the comment colomn.
    if (in_array('comment', $this->info['schema_fields_sql']['base table'])) {
      $info['comment'] = array(
        'label' => t('Comments allowed'),
        'description' => 'Whether comments are allowed on this entity' . $rev_data . ': 0 = no, 1 = closed (read only), 2 = open (read/write).',
        'type' => 'integer',
        'schema field' => 'comment',
      );
    }

    // Rewrite the status colomn.
    if (in_array('status', $this->info['schema_fields_sql']['base table'])) {
      $info['status'] = array(
        'label' => t(' Status'),
        'description' => 'Boolean indicating whether the entity' . $rev_data . ' is published (visible to non-administrators).',
        'type' => 'integer',
        'options list' => 'entity_metadata_status_options_list',
        'setter callback' => 'entity_property_verbatim_set',
        'schema field' => 'status',
      ) + $setter_permission;
    }

    // Rewrite the promote colomn.
    if (in_array('promote', $this->info['schema_fields_sql']['base table'])) {
      $info['promote'] = array(
        'label' => t('Promoted to frontpage'),
        'description' => 'Boolean indicating whether the entity' . $rev_data . ' should be displayed on the front page.',
        'type' => 'boolean',
        'setter callback' => 'entity_property_verbatim_set',
        'schema field' => 'promote',
      ) + $setter_permission;
    }

    // Rewrite the sticky colomn.
    if (in_array('sticky', $this->info['schema_fields_sql']['base table'])) {
      $info['sticky'] = array(
        'label' => t('Sticky in lists'),
        'description' => 'Boolean indicating whether the entity' . $rev_data . ' should be displayed at the top of lists in which it appears.',
        'type' => 'boolean',
        'setter callback' => 'entity_property_verbatim_set',
        'schema field' => 'sticky',
      ) + $setter_permission;
    }

    // Rewrite the weight colomn.
    if (in_array('weight', $this->info['schema_fields_sql']['base table'])) {
      $info['weight'] = array(
        'label' => t('Weight'),
        'description' => t('A value for sorting %types.', $arg_desc),
        'type' => 'integer',
        'setter callback' => 'entity_property_verbatim_set',
        'schema field' => 'weight',
      );
    }
    // Rewrite the description colomn.
    if (in_array('description', $this->info['schema_fields_sql']['base table'])) {
      $info['description'] = array(
        'label' => t('Description'),
        'description' => t('A brief description of this %type.', $arg_desc),
        'type' => 'text',
        'setter callback' => 'entity_property_verbatim_set',
        'schema field' => 'description',
      );
    }
    // Rewrite the help colomn.
    if (in_array('help', $this->info['schema_fields_sql']['base table'])) {
      $info['help'] = array(
        'label' => t('Help'),
        'description' => t('Help information shown to the user when creating a content of this %type.', $arg_desc),
        'type' => 'text',
        'setter callback' => 'entity_property_verbatim_set',
        'schema field' => 'help',
      );
    }
    // Rewrite the title_label colomn.
    if (in_array('title_label', $this->info['schema_fields_sql']['base table'])) {
      $info['title_label'] = array(
        'label' => t('Title label'),
        'description' => t('Title label shown to the user when creating a content of this %type.', $arg_desc),
        'type' => 'text',
        'setter callback' => 'entity_property_verbatim_set',
        'schema field' => 'title_label',
      );
    }
    // export colomns don't need to be provided in token, rules, etc...
    if (in_array('export_status', $this->info['schema_fields_sql']['base table']) && isset($info['export_status'])) {
      unset($info['export_status']);
    }
    if (in_array('export_module', $this->info['schema_fields_sql']['base table']) && isset($info['export_module'])) {
      unset($info['export_module']);
    }

    // add property is_new
    $info['is_new'] = array(
      'label' => t('Is new'),
      'description' => t('Whether the %type is new and not saved to the database yet.', $arg_desc),
      'type' => 'boolean',
      'getter callback' => 'entity_metadata_customizable_entities_get_properties',
    );

    // add property edit_url
    $info['edit_url'] = array(
      'label' => t('Edit URL'),
      'description' => t("The URL of the %type's edit page.", $arg_desc),
      'type' => 'uri',
      'getter callback' => 'entity_metadata_customizable_entities_get_properties',
      'computed' => TRUE,
    );

    // @TODO column data ?

    // Save it in the reference variable.
    $property_info[$this->type]['properties'] = $info;
  }

  /**
   * Provided entity_property_info() informations for custom fields.
   *
   * Each class should implement it.
   *
   * @return
   *   An array, the DP entity_property_info() fields.
   *
   * @see CustomizableEntitiesEntityInfo::entity_property_info()
   */
  protected function entityPropertyInfo_custom_fields($custom_properties) {
    // Alter $custom_properties.

    foreach ($custom_properties as $col => $info) {
      if (!empty($info['schema field'])) {
        if (!isset($info['getter callback'])) {
          $custom_properties[$col]['getter callback'] = 'entity_property_verbatim_get';
        }
        if (!isset($info['setter callback'])) {
          $custom_properties[$col]['setter callback'] = 'entity_property_verbatim_set';
        }
      }
    }
    return $custom_properties;
  }
}

/**
 * Default controller for generating extra fields based on property metadata.
 *
 * By default a display extra field for each property not being a field, ID or
 * bundle is generated.
 */
class CustomizableEntitiesExtraFieldsController extends EntityDefaultExtraFieldsController {
  protected $ce_fields = array();
  protected $disable_extrafields = array();

  public function __construct($type) {
    parent::__construct($type);

    // Set the customizable entities fields names.
    $this->ce_fields = cutomizable_entities_defined_properties($this->entityInfo, TRUE);

    // Set $this->disable_extrafields.
    if (!empty($this->propertyInfo['properties']) && is_array($this->propertyInfo['properties'])) {
      foreach ($this->propertyInfo['properties'] as $name => $property_info) {

        // Computed
        if ($name == 'is_new') {
          //$this->disable_extrafields[$this->entityType]['form'][] = $name;
          $this->disable_extrafields[$this->entityType]['display'][] = $name;
        }
        if ($name == 'url') {
          //$this->disable_extrafields[$this->entityType]['form'][] = $name;
          $this->disable_extrafields[$this->entityType]['display'][] = $name;
        }
        if ($name == 'edit_url') {
          //$this->disable_extrafields[$this->entityType]['form'][] = $name;
          $this->disable_extrafields[$this->entityType]['display'][] = $name;
        }
        if ($name == 'revision') {
          // This property has its onwn fieldset.
          $this->disable_extrafields[$this->entityType]['form'][] = $name;
          // Admin stuff, don't dispaly it on display page.
          $this->disable_extrafields[$this->entityType]['display'][] = $name;
        }

        // Entity keys
        if (!empty($this->entityInfo['entity keys']['id']) && $name == $this->entityInfo['entity keys']['id']) {
          // This property is hidden on edit form.
          $this->disable_extrafields[$this->entityType]['form'][] = $name;
          // Admin stuff, don't dispaly it on display page.
          $this->disable_extrafields[$this->entityType]['display'][] = $name;
        }
        if (!empty($this->entityInfo['entity keys']['label']) && $name == $this->entityInfo['entity keys']['label']) {
          // Show title on edit and display pages.
        }
        if (!empty($this->entityInfo['entity keys']['bundle']) && $name == $this->entityInfo['entity keys']['bundle']) {
          // This property is hidden on edit form.
          $this->disable_extrafields[$this->entityType]['form'][] = $name;
        }
        // Always hide export status and module
        $export_module = (!empty($this->entityInfo['entity keys']['module'])) ? $this->entityInfo['entity keys']['module'] : 'export_module';
        $export_status = (!empty($this->entityInfo['entity keys']['status'])) ? $this->entityInfo['entity keys']['status'] : 'export_status';
        if (isset($this->disable_extrafields[$this->entityType]['form']) && !in_array($export_module, $this->disable_extrafields[$this->entityType]['form'])) {
          $this->disable_extrafields[$this->entityType]['form'][] = $export_module;
          $this->disable_extrafields[$this->entityType]['display'][] = $export_module;
        }
        if (isset($this->disable_extrafields[$this->entityType]['form']) && !in_array($export_status, $this->disable_extrafields[$this->entityType]['form'])) {
          $this->disable_extrafields[$this->entityType]['form'][] = $export_status;
          $this->disable_extrafields[$this->entityType]['display'][] = $export_status;
        }


        if (!empty($this->entityInfo['entity keys']['revision']) && $name == $this->entityInfo['entity keys']['revision']) { // vid.
          // This property is hidden on edit form
          $this->disable_extrafields[$this->entityType]['form'][] = $name;
          // Admin stuff, don't dispaly it on display page.
          $this->disable_extrafields[$this->entityType]['display'][] = $name;
        }

        // Revisions
        if ($name == 'log') {
          // This property has its onwn fieldset.
          $this->disable_extrafields[$this->entityType]['form'][] = $name;
        }

        if ($name == 'created') {
          // This property is hidden on edit form.
          $this->disable_extrafields[$this->entityType]['form'][] = $name;
        }

        if ($name == 'changed') {
          // This property has its onwn fieldset.
          $this->disable_extrafields[$this->entityType]['form'][] = $name;
        }
        if ($name == 'uid') {
          // This property has its onwn fieldset.
          $this->disable_extrafields[$this->entityType]['form'][] = $name;
        }
        if ($name == 'language') {
          // Show language on edit and display pages.
        }
        if ($name == 'promote') {
          // This property has its onwn fieldset.
          $this->disable_extrafields[$this->entityType]['form'][] = $name;
        }
        if ($name == 'sticky') {
          // This property has its onwn fieldset.
          $this->disable_extrafields[$this->entityType]['form'][] = $name;
        }
        if ($name == 'status') {
          // This property has its onwn fieldset.
          $this->disable_extrafields[$this->entityType]['form'][] = $name;
        }
        if ($name == 'comment') {
          // Comment is not implemented yet.
          $this->disable_extrafields[$this->entityType]['form'][] = $name;
          $this->disable_extrafields[$this->entityType]['display'][] = $name;
        }
        if ($name == 'title_label') {
          // This property has its onwn fieldset.
          $this->disable_extrafields[$this->entityType]['form'][] = $name;
        }
        if ($name == 'help') {
          // This property has its onwn fieldset.
          $this->disable_extrafields[$this->entityType]['form'][] = $name;
        }
        if ($name == 'weight') {
          // Admin stuff, don't dispaly it on display page.
          $this->disable_extrafields[$this->entityType]['display'][] = $name;
        }
        if ($name == 'data') {
          // Admin stuff, don't display it.
          $this->disable_extrafields[$this->entityType]['form'][] = $name;
          $this->disable_extrafields[$this->entityType]['display'][] = $name;
        }

        // Do not display computed datas on edit form.
        if (empty($property_info['schema field'])) {
          $this->disable_extrafields[$this->entityType]['form'][] = $name;
        }
      }
    }
  }

  public function fieldExtraFields() {
    $extra = array();
    // Handle bundle properties.
    // Provide extra fields even if the entity is not fiedldable. the interface
    // for managing thoses extra fields is added into the controller ui class :
    // CustomizableEntitiesEntityUIController::hook_menu().
    $custom_properties = array();
    $ce_properties = array();
    foreach ($this->entityInfo['bundles'] as $bundle_name => $info) {
      if (!empty($this->propertyInfo['properties']) && is_array($this->propertyInfo['properties'])) {
        foreach ($this->propertyInfo['properties'] as $name => $property_info) {
          if (!in_array($name, $this->disable_extrafields[$this->entityType]['form'])) {
            $extra[$this->entityType][$bundle_name]['form'][$name] = $this->generateExtraFieldInfo($name, $property_info);
          }
          if (!in_array($name, $this->disable_extrafields[$this->entityType]['display'])) {
            $extra[$this->entityType][$bundle_name]['display'][$name] = $this->generateExtraFieldInfo($name, $property_info);
          }
          // Find custom colomns.
          // split properties between 2 categories.
          if (!in_array($name, $this->ce_fields)) {
            // This property is not defined by the customizable_entities module.
            foreach (array('form', 'display') as $mode) {
              if (isset($extra[$this->entityType][$bundle_name][$mode][$name])) {
                $custom_properties[$this->entityType][$bundle_name][$mode][$name] = $extra[$this->entityType][$bundle_name][$mode][$name];
              }
            }
          }
          else {
            // This property is defined by the customizable_entities module.
            foreach (array('form', 'display') as $mode) {
              if (isset($extra[$this->entityType][$bundle_name][$mode][$name])) {
                $ce_properties[$this->entityType][$bundle_name][$mode][$name] = $extra[$this->entityType][$bundle_name][$mode][$name];
              }
            }
          }
        }
      }
    }

    // Alter the custom properties.
    $custom_properties = $this->fieldExtraFields_custom($custom_properties);
    // Overvrite extra :
    $extra = drupal_array_merge_deep($ce_properties, $custom_properties);

    // Set default settings on extra fields
    if (!empty($this->entityInfo['customizable entities']['install class'])) {
      $class = $this->entityInfo['customizable entities']['install class'];
      $method = 'bundle_settings_default';
      $args = array($this->entityType, array($this->entityType => $this->entityInfo));
      $args_method = array($extra);
      customizable_entities_call_method($class, $method, $args, $args_method);
    }
    return $extra;
  }

  /**
   * Extra field definition for custom properties
   *
   * If returns array() : the custom properties will not be displayed.
   */
  protected function fieldExtraFields_custom($custom) {
    return $custom;
  }
}

