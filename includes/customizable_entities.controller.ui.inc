<?php
/**
 * Default controller for providing UI.
 *
 * Extend the common class 'EntityDefaultUIController' for entities.
 */
class CustomizableEntitiesEntityUIController extends EntityDefaultUIController {
  // CRUD info for building menu. They are build from permissions and overwrited
  // by the child class.
  protected $allow_create = NULL;
  protected $allow_update = NULL;
  protected $allow_view = NULL;
  protected $allow_delete = NULL;
  protected $allow_clone = TRUE;
  protected $has_view_page = NULL;
  protected $hide_export_status = FALSE;

  /**
   * retrieve crud values from the permissions.
   */
  public function __construct($entity_type, $entity_info) {
    parent::__construct($entity_type, $entity_info);
    $permissions = $this->entityInfo['customizable entities']['crud'];
    $crud = array('create', 'update', 'view', 'delete');
    foreach ($crud as $op) {
      $variable = "allow_{$op}";
      if ($this->$variable === NULL) {
        $this->$variable = (!empty($permissions[$op])); // TRUE or FALSE.)
      }
    }

    // For Bundle : As an exemple, node types have no view page, there "view"
    // page are redirected to edit page.
    if ($this->has_view_page === NULL) {
      if (!$this->allow_view && $this->allow_update) {
        $this->has_view_page = FALSE;
      }
      else {
        $this->has_view_page = TRUE;
        //$this->has_view_page = (empty($this->entityInfo['bundle of']));
      }
    }
    if ($this->hide_export_status === NULL) {
      $this->hide_export_status = !(empty($this->entityInfo['bundle of']));
    }
  }

  /**
   * Provides definitions for implementing hook_menu().
   *
   * Manage entity view page, adding, synchronize (one and all) pages, revisions,
   * import, clone, devel pages.
   */
  public function hook_menu() {

    // In order to works with the menu router, the modules must be called in
    // this order : bundle, the entity of this bundle, depending on the
    // order in the hook_entity_info() functions are called.

    $items = array();
    $crud_path = $this->entityInfo['customizable entities']['crud path'];
    $overview_path = $this->entityInfo['customizable entities']['overview path'];
    $fields_path = (!empty($this->entityInfo['admin ui']['path'])) ? $this->entityInfo['admin ui']['path'] : FALSE;
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%entity_object';
    $plural_label = isset($this->entityInfo['plural label']) ? $this->entityInfo['plural label'] : $this->entityInfo['label'] . 's';

    $file_page = array(
      'file path' => drupal_get_path('module', 'customizable_entities') . '/pages',
      'file' => 'customizable_entities_entity.pages.inc',
    );

    if (!empty($overview_path)) {
      $items[$overview_path] = array(
        // Uppercase for first letter for title.
        'title' => drupal_strtoupper(drupal_substr($plural_label, 0, 1)) . drupal_substr($plural_label, 1),
        'page callback' => 'drupal_get_form',
        // Use a special form builder, instead of the class one.
        'page arguments' => array('customizable_entities_admin_content', $this->entityType),
        'description' => 'Manage ' . $plural_label . '.',
        'access callback' => 'entity_access',
        'access arguments' => array('view', $this->entityType),
        'file path' => drupal_get_path('module', 'customizable_entities') . '/pages',
        'file' => 'customizable_entities_entity_overview.pages.inc',
        //'page arguments' => array($this->entityType . '_overview_form', $this->entityType),
        //'file' => 'includes/entity.ui.inc',
      );
      $items[$overview_path . '/list'] = array(
        'title' => 'List',
        'type' => MENU_DEFAULT_LOCAL_TASK,
        'weight' => -10,
      );
      if ($this->entityInfo['customizable entities']['overview path'] == "admin/content/{$this->entityType}") {
        // A default tab already exists.
        unset($items[$overview_path . '/list']);
        $items[$overview_path]['type'] = MENU_LOCAL_TASK;
        // Move this tab after the content and comment tabs.
        $items[$overview_path]['weight'] = 2;
      }
    }

    if (!empty($crud_path)) {

      // Handle import
      if (!empty($this->entityInfo['exportable'])) {
        // Menu item for importing an entity.
        $items["{$crud_path}/import"] = array(
          'title callback' => 'entity_ui_get_action_title',
          'title arguments' => array('import', $this->entityType),
          'page callback' => 'drupal_get_form',
          'page arguments' => array($this->entityType . '_operation_form', $this->entityType, NULL, 'import'),
          'access callback' => 'entity_access',
          'access arguments' => array('create', $this->entityType),
          'file' => 'includes/entity.ui.inc',
          'type' => MENU_LOCAL_ACTION,
        );
        // Hide "/import" into the menu.
        $items["{$crud_path}/import"]['type'] = MENU_CALLBACK;
      }

      // Handle Add entity :
      if ($this->allow_create) {
        $has_bundles = !empty($this->entityInfo['entity keys']['bundle']);
        if ($has_bundles) {
          // Create direct link if there is only one bundle.
          $items["{$crud_path}/add"] = array(
            'title' => 'Add ' . $this->entityInfo['label'],
            'page callback' => 'customizable_entities_add_page',
            'page arguments' => array($this->entityType),
            'access callback' => '_customizable_entities_entity_bundle_add_access',
            'access arguments' => array($this->entityType, NULL),
            //'type' => MENU_LOCAL_ACTION,
          ) + $file_page;

          // Create an add menu item for each bundle.
          $bundle_entity_type = customizable_entities_entity_find_bundle_entity_type($this->entityType);
          foreach ($this->entityInfo['bundles'] as $type => $datas) {
            $type_url_str = str_replace('_', '-', $type);
            $path_add = "{$crud_path}/add/{$type_url_str}";
            $items[$path_add] = array(
              'title callback' => 'entity_ui_get_action_title',
              'title arguments' => array('add', $this->entityType, $type),
              // Use my callback for auto setting form.
              'page callback' => 'customizable_entities_entity_entity_ui_get_form',
              'page arguments' => array($this->entityType, NULL, 'add', array(), $type),
              'access callback' => '_customizable_entities_entity_bundle_add_access',
              'access arguments' => array($this->entityType, $type),
              'type' => MENU_NORMAL_ITEM, //MENU_LOCAL_ACTION,
            ) + $file_page;
            $bundles = entity_load_multiple_by_name($bundle_entity_type, array($type));
            $bundle = (!empty($bundles)) ? $bundles[$type] : FALSE;
            if ($bundle && !empty($bundle->description)) {
              $items[$path_add]['description'] = check_plain($bundle->description);
            }
          }
        }
        else {
          $items["{$crud_path}/add"] = array(
            'title callback' => 'entity_ui_get_action_title',
            'title arguments' => array('add', $this->entityType),
            // Use my callback for auto setting form.
            'page callback' => 'customizable_entities_entity_entity_ui_get_form',
            'page arguments' => array($this->entityType, NULL, 'add'),
            'access callback' => 'entity_access',
            'access arguments' => array('create', $this->entityType),
            'type' => MENU_LOCAL_ACTION,
          ) + $file_page;
        }
      }

      $path_view = "{$crud_path}/{$wildcard}";
      $id_count = count(explode('/', $path_view));
      $placeholder_view = $id_count - 1;

      if ($this->allow_view) {
        $items[$path_view] = array(
          'title callback' => 'entity_label',
          'title arguments' => array($this->entityType, $placeholder_view),
          'access callback' => 'entity_access',
          'access arguments' => array('view', $this->entityType, $placeholder_view),
          'page callback' => 'customizable_entities_entity_view_page',
          'page arguments' => array($this->entityType, $placeholder_view),
          'load arguments' => array($this->entityType),
          'description' => "View {$this->entityInfo['label']} page",
          'type' => MENU_CALLBACK,
        ) + $file_page;

        $items["{$path_view}/view"] = array(
          'title' => 'view',
          'weight' => -10,
          'type' => MENU_DEFAULT_LOCAL_TASK,
        );
      }

      if ($this->allow_update) {
        $items["{$path_view}/edit"] = array(
          'title' => 'Edit',
          'page callback' => 'customizable_entities_entity_entity_ui_get_form',
          'page arguments' => array($this->entityType, $placeholder_view),
          'load arguments' => array($this->entityType),
          'access callback' => 'entity_access',
          'access arguments' => array('update', $this->entityType, $placeholder_view),
          'type' => MENU_LOCAL_TASK,
        ) + $file_page;
      }

      if (!$this->has_view_page && isset($items["{$path_view}/edit"])) {
        $items[$path_view] = $items["{$path_view}/edit"];
        //$items[$path_view]['type'] = MENU_CALLBACK;
        unset($items[$path_view]['type'], $items[$path_view]['title']);
        $items[$path_view]['title callback'] = 'entity_label';
        $items[$path_view]['title arguments'] = array($this->entityType, $placeholder_view);
        unset($items["{$path_view}/view"]);
        $items["{$path_view}/edit"] = array(
          'title' => 'Edit',
          'load arguments' => array($this->entityType),
          'type' => MENU_DEFAULT_LOCAL_TASK,
        );
      }

      if ($this->allow_clone) {
        $items["{$path_view}/clone"] = array(
          'title' => 'Clone',
          'page callback' => 'customizable_entities_entity_entity_ui_get_form',
          'page arguments' => array($this->entityType, $placeholder_view, 'clone'),
          'load arguments' => array($this->entityType),
          'access callback' => 'entity_access',
          'access arguments' => array('create', $this->entityType),
          // Remove from the menu.
          'type' => MENU_CALLBACK,
        ) + $file_page;
      }

      if ($this->allow_delete) {
        // Act for delete and revert.
        $items["{$path_view}/%"] = array(
          'page callback' => 'drupal_get_form',
          'page arguments' => array($this->entityType . '_operation_form', $this->entityType, $placeholder_view, $placeholder_view + 1),
          'load arguments' => array($this->entityType), // need to remove this ?
          'access callback' => 'entity_access',
          'access arguments' => array('delete', $this->entityType, $placeholder_view),
          'file' => 'includes/entity.ui.inc',
          // Remove from the menu.
          'type' => MENU_CALLBACK,
        );
      }

      // Handle revision stuff.
      if (!empty($this->entityInfo['entity keys']['revision'])) {
        $revision_file = array(
          'file path' => drupal_get_path('module', 'customizable_entities') . '/pages',
          'file' => 'customizable_entities_revision.pages.inc',
        );
        $items[$path_view . '/revisions'] = array(
          'title' => 'Revisions',
          'page callback' => 'customizable_entities_entity_revision_overview',
          'page arguments' => array($this->entityType, $placeholder_view),
          'load arguments' => array($this->entityType),
          'access callback' => '_customizable_entities_entity_revision_access',
          'access arguments' => array($this->entityType, $placeholder_view),
          'weight' => 2,
          'type' => MENU_LOCAL_TASK,
        ) + $revision_file;

        // Too bad the %entity_revision wildcard doesn't work. It may be because
        // of the previous %entity_object wildcard. I manage it in the
        // _customizable_entities_entity_revision_access() function.
        $items[$path_view . '/revisions/%/view'] = array(
          'title' => 'Revisions',
          'load arguments' => array($this->entityType, $placeholder_view + 2),
          'page callback' => 'customizable_entities_entity_show',
          'page arguments' => array($this->entityType, $placeholder_view + 2, TRUE),
          'access callback' => '_customizable_entities_entity_revision_access',
          'access arguments' => array($this->entityType, $placeholder_view + 2),
        ) + $revision_file;
        $items[$path_view . '/revisions/%/revert'] = array(
          'title' => 'Revert to earlier revision',
          'load arguments' => array($this->entityType, $placeholder_view + 2),
          'page callback' => 'drupal_get_form',
          'page arguments' => array('customizable_entities_entity_revision_revert_confirm', $this->entityType, $placeholder_view + 2),
          'access callback' => '_customizable_entities_entity_revision_access',
          'access arguments' => array($this->entityType, $placeholder_view + 2, 'update'),
        ) + $revision_file;
        $items[$path_view . '/revisions/%/delete'] = array(
          'title' => 'Delete earlier revision',
          'load arguments' => array($this->entityType, $placeholder_view + 2),
          'page callback' => 'drupal_get_form',
          'page arguments' => array('customizable_entities_entity_revision_delete_confirm', $this->entityType, $placeholder_view + 2),
          'access callback' => '_customizable_entities_entity_revision_access',
          'access arguments' => array($this->entityType, $placeholder_view + 2, 'delete'),
        ) + $revision_file;
      }

      // Set devel module with wildcard.
      if (module_exists('devel')) {
        $devel_path = drupal_get_path('module', 'devel');
        $items[$path_view . '/devel'] = array(
          'title' => 'Devel',
          'page callback' => 'devel_load_object',
          'page arguments' => array($this->entityType, $placeholder_view),
          'access arguments' => array('access devel information'),
          'load arguments' => array($this->entityType),
          'type' => MENU_LOCAL_TASK,
          'file' => 'devel.pages.inc',
          'weight' => 100,
          'file path' => $devel_path,
        );
        $items[$path_view . '/devel/load'] = array(
          'title' => 'Load',
          'type' => MENU_DEFAULT_LOCAL_TASK,
        );
        // @TODO : render page fails because it calls customizable_entities_token_view()
        // function that not exists (fata error).
        $items[$path_view . '/devel/render'] = array(
          'title' => 'Render',
          'page callback' => 'devel_render_object',
          // For answer and token, provide 'customizable_entities_token' instead of
          // 'customizable_entities_token_xxxxxx' for calling customizable_entities_token_view().
          'page arguments' => array($this->entityType, $placeholder_view, $this->entityType),
          'access arguments' => array('access devel information'),
          'load arguments' => array($this->entityType),
          'file' => 'devel.pages.inc',
          'type' => MENU_LOCAL_TASK,
          'file path' => $devel_path,
          'weight' => 100,
        );
      }
    }

    // If the entity is not fieldable, we still want to define admin fields and
    // manage interface for extra fields
    if ($fields_path && empty($this->entityInfo['fieldable'])) {
      // copy from the field_ui_menu()
      // Extract access information, providing defaults.

      $access = array();
      $access = $access + array(
        'access callback' => 'user_access',
        'access arguments' => array('administer fields'),
      );

      if ($fields_path) {
        if ($this->allow_update) {
          $items["$fields_path/fields"] = array(
            'title' => 'Manage properties', // Instead of 'Manage fields'.
            'page callback' => 'drupal_get_form',
            'page arguments' => array('field_ui_field_overview_form', $this->entityType, $this->entityType),
            'type' => MENU_LOCAL_TASK,
            'weight' => 1,
            'file path' => drupal_get_path('module', 'field_ui'),
            'file' => 'field_ui.admin.inc',
          ) + $access;
        }
        // 'Manage display' tab.
        if ($this->allow_view) {
          $items["$fields_path/display"] = array(
            'title' => 'Manage display',
            'page callback' => 'drupal_get_form',
            'page arguments' => array('field_ui_display_overview_form', $this->entityType, $this->entityType, 'default'),
            'type' => MENU_LOCAL_TASK,
            'weight' => 2,
            'file' => 'field_ui.admin.inc',
            'file path' => drupal_get_path('module', 'field_ui'),
          ) + $access;

          // View modes secondary tabs.
          // The same base $fields_path for the menu item (with a placeholder) can be
          // used for all bundles of a given entity type; but depending on
          // administrator settings, each bundle has a different set of view
          // modes available for customisation. So we define menu items for all
          // view modes, and use an access callback to determine which ones are
          // actually visible for a given bundle.
          $weight = 0;
          $view_modes = array('default' => array('label' => t('Default'))) + $this->entityInfo['view modes'];
          foreach ($view_modes as $view_mode => $view_mode_info) {
            $items["$fields_path/display/$view_mode"] = array(
              'title' => $view_mode_info['label'],
              'page arguments' => array('field_ui_display_overview_form', $this->entityType, $this->entityType, $view_mode),
              // The access callback needs to check both the current 'custom
              // display' setting for the view mode, and the overall access
              // rules for the bundle admin pages.
              'access callback' => '_field_ui_view_mode_menu_access',
              'access arguments' => array_merge(array($this->entityType, $this->entityType, $view_mode, $access['access callback']), $access['access arguments']),
              'type' => ($view_mode == 'default' ? MENU_DEFAULT_LOCAL_TASK : MENU_LOCAL_TASK),
              'weight' => ($view_mode == 'default' ? -10 : $weight++),
              'file path' => drupal_get_path('module', 'field_ui'),
              'file' => 'field_ui.admin.inc',
            );
          }
        }
      }
    }

    return $items;
  }

  public function hook_forms() {
    $forms = parent::hook_forms();
    // Add form from the customizable_entities module. customizable_entities entities will not have to declare a
    // $this->entityType . '_form' function to be edited.
    $forms[$this->entityType . '_form']['callback'] = '_customizable_entities_form';
    // Handle bundle // $entity_type . '_edit_' . $bundle . '_form'
    if (!empty($this->entityInfo['entity keys']['bundle'])) {
      $funct = (function_exists($this->entityType . '_form')) ? $this->entityType . '_form' : '_customizable_entities_form';
      foreach ($this->entityInfo['bundles'] as $bundle => $datas) {
        $form_id = $this->entityType . '_edit_' . $bundle . '_form';
        $forms[$form_id]['callback'] = $funct;
      }
    }
    return $forms;
  }

  public function operationForm($form, &$form_state, $entity, $op) {
    // On deleting entities, add an explicit message about deleting bundle or
    // source entity.
    switch ($op) {
      case 'delete':
        $label = entity_label($this->entityType, $entity);
        $confirm_question = t('Are you sure you want to delete the %entity %label?', array('%entity' => $this->entityInfo['label'], '%label' => $label));
        if (empty($form_state['input']['confirm'])) {
          customizable_entities_warning_deletion_message($this->entityType, $entity);
        }
        return confirm_form($form, $confirm_question, $this->path);

    }
    parent::operationForm($form, $form_state, $entity, $op);
  }

  /**
   * Generates the table headers for the overview table.
   */
  protected function overviewTableHeaders($conditions, $rows, $additional_header = array()) {

    $header = $additional_header;

    // Add the type colonne.
    $has_bundles = !empty($this->entityInfo['entity keys']['bundle']);
    if ($has_bundles) {
      $header[] = t('type');
    }

    $has_author = in_array('uid', $this->entityInfo['schema_fields_sql']['base table']);
    if ($has_author) {
      $header[] = t('User');
    }

    $has_active = in_array('status', $this->entityInfo['schema_fields_sql']['base table']);
    if ($has_active) {
      $header[] = t('Status');
    }

    $has_date = in_array('changed', $this->entityInfo['schema_fields_sql']['base table']);
    if ($has_date) {
      $header[] = t('Updated');
    }

    $has_language = in_array('language', $this->entityInfo['schema_fields_sql']['base table']);
    if ($has_language) {
      // Enable language column if translation module is enabled or if we have any
      // node with language.
      $multilanguage = (module_exists('translation') || db_query_range('SELECT 1 FROM {' . $this->entityInfo['base table'] . '} WHERE language <> :language', 0, 1, array(':language' => LANGUAGE_NONE))->fetchField());
      if ($multilanguage) {
        $header[] = t('Language');
      }
    }

    array_unshift($header, t('Label'));
    if (!empty($this->entityInfo['exportable']) && !$this->hide_export_status) {
      $header[] = t('Export status');
    }
    // Add operations with the right colspan.
    $header[] = array(
      'data' => t('Operations'),
      'colspan' => $this->operationCount(),
    );
    return $header;
  }

  /**
   * Generates the row for the passed entity and may be overridden in order to
   * customize the rows.
   *
   * Add the synchronization status and LimeSurvey status colomns and the
   * synchronize operation. Delete export status colomn and edit operation
   * depending on settings ($hide_export_status and $allow_update).
   *
   * @param $additional_cols
   *   Additional columns to be added after the entity label column.
   */
  protected function overviewTableRow($conditions, $id, $entity, $additional_cols = array(), $additional_ops = array()) {

    // Disable stuff.
    //return parent::overviewTableRow($conditions, $id, $entity, $additional_cols);
    // Rewrite te whole method, since all links changes.
    if (!is_numeric($id)) {
      $id = str_replace('_', '-', $id);
    }

    $row = array();

    $entity_uri = entity_uri($this->entityType, $entity);

    $row[] = array(
      'data' => array(
        '#theme' => 'entity_ui_overview_item',
        '#label' => entity_label($this->entityType, $entity),
        '#name' => !empty($this->entityInfo['exportable']) ? entity_id($this->entityType, $entity) : FALSE,
        '#url' => $entity_uri ? $entity_uri : FALSE,
        '#entity_type' => $this->entityType,
      ),
    );

    // Add in any passed additional cols.
    foreach ($additional_cols as $col) {
      $row[] = $col;
    }

    $has_bundles = !empty($this->entityInfo['entity keys']['bundle']);
    if ($has_bundles) {
      $type = $entity->{$this->entityInfo['entity keys']['bundle']};
      $type_founded = FALSE;
      foreach ($this->entityInfo['bundles'] as $bundle_type => $bundle_datas) {
        if ($bundle_type == $type) {
          $row[] = l($bundle_datas['label'], $bundle_datas['admin']['real path']);
          $type_founded = TRUE;
          break;
        }
      }
      if (!$type_founded) {
        // The bundle has been deleted.
        $row[] = t('@bundle (deleted)', array('@bundle' => $type));
      }
    }
    $has_author = in_array('uid', $this->entityInfo['schema_fields_sql']['base table']);
    if ($has_author) {
      $uid = $entity->uid;
      $account = user_load($uid);
      $name =  format_username($account);
      $user_uri = (!empty($account->uid) && user_access('View user profiles')) ? "user/{$account->uid}" : FALSE;
      $row[] = $user_uri ? l($name, $user_uri) : $name;
    }

    $has_active = in_array('status', $this->entityInfo['schema_fields_sql']['base table']);
    if ($has_active) {
      $row[] = $entity->status ? t('published') : t('not published');
    }

    $has_date = in_array('changed', $this->entityInfo['schema_fields_sql']['base table']);
    if ($has_date) {
      $row[] = format_date($entity->changed, 'short');
    }

    $has_language = in_array('language', $this->entityInfo['schema_fields_sql']['base table']);
    if ($has_language) {
      $multilanguage = (module_exists('translation') || db_query_range('SELECT 1 FROM {' . $this->entityInfo['base table'] . '} WHERE language <> :language', 0, 1, array(':language' => LANGUAGE_NONE))->fetchField());
      if ($multilanguage) {
        $languages = language_list();
        $langcode = entity_language($this->entityType, $entity);
        if ($langcode == LANGUAGE_NONE || isset($languages[$langcode])) {
          $row[] = $langcode == LANGUAGE_NONE ? t('Language neutral') : t($languages[$langcode]->name);
        }
        else {
          $row[] = t('Undefined language (@langcode)', array('@langcode' => $langcode));
        }
      }
    }

    // Add a row for the exportable status.
    if (!empty($this->entityInfo['exportable']) && !$this->hide_export_status) {
      $row[] = array('data' => array(
          '#theme' => 'entity_status',
          '#status' => $entity->{$this->statusKey},
        ));
    }

    foreach ($additional_ops as $op) {
      $row[] = $op;
    }

    // In case this is a bundle, we add links to the field ui tabs.
    $field_ui = !empty($this->entityInfo['bundle of']) && entity_type_is_fieldable($this->entityInfo['bundle of']) && module_exists('field_ui');
    // For exportable entities we add an export link.
    $exportable = !empty($this->entityInfo['exportable']);
    // If i18n integration is enabled, add a link to the translate tab.
    $i18n = !empty($this->entityInfo['i18n controller class']);

    // Set a view path instead of admin path:
    $path_view = $this->entityInfo['customizable entities']['crud path'];

    // Add operations depending on the status.
    if (entity_has_status($this->entityType, $entity, ENTITY_FIXED)) {
      $row[] = array(
        'data' => l(t('clone'), $path_view . '/' . $id . '/clone'),
        'colspan' => $this->operationCount(),
      );
    }
    else {
      // Delete edit link.
      if ($this->allow_update) {
        $row[] = l(t('edit'), $path_view . '/' . $id . '/edit');
      }
      if ($field_ui) {
        $row[] = l(t('manage fields'), $this->path . '/manage/' . $id . '/fields');
        $row[] = l(t('manage display'), $this->path . '/manage/' . $id . '/display');
      }
      if ($i18n) {
        $row[] = l(t('translate'), $path_view . '/' . $id . '/translate');
      }
      // Cloning a entry may be a problem for linking drupal and limesurvey...
      if ($exportable) {
        $row[] = l(t('clone'), $path_view . '/' . $id . '/clone');
      }

      if (empty($this->entityInfo['exportable']) || !entity_has_status($this->entityType, $entity, ENTITY_IN_CODE)) {
        $row[] = l(t('delete'), $path_view . '/' . $id . '/delete', array('query' => drupal_get_destination()));
      }
      elseif (entity_has_status($this->entityType, $entity, ENTITY_OVERRIDDEN)) {
        $row[] = l(t('revert'), $path_view . '/' . $id . '/revert', array('query' => drupal_get_destination()));
      }
      else {
        $row[] = '';
      }
    }
    if ($exportable) {
      $row[] = l(t('export'), $path_view . '/' . $id . '/export');
    }

    return $row;
  }

  /**
   * Returns the operation count for calculating colspans.
   */
  protected function operationCount() {

    $count = parent::operationCount();
    $count -= $this->allow_update ? 0 : 1;
    return $count;
  }
}


