<?php
/**
 * @file
 * Holds customizable_entities module's theme functions.
 */

/**
 * Process variables for customizable_entities.tpl.php.
 */
function template_preprocess_customizable_entities(&$variables) {
  // First set data from the entity module.
  module_load_include('inc', 'entity', 'theme/entity.theme');
  template_preprocess_entity($variables);

  // Add my own datas.
  $entity_type = $variables['elements']['#entity_type'];
  $entity = $variables['elements']['#entity'];
  $info = entity_get_info($entity_type);

  // Provide a distinct $teaser boolean.
  $variables['teaser'] = $variables['view_mode'] == 'teaser';

  $has_author = in_array('uid', $info['schema_fields_sql']['base table']);
  $has_date = in_array('created', $info['schema_fields_sql']['base table']);
  $variables['date'] = ($has_date) ? format_date($entity->created) : '';
  if ($has_author) {
    $account = user_load($entity->uid);
  }
  $variables['name'] = (isset($account)) ? theme('username', array('account' => $account)) : '';

  // Display post information only on certain node types.
  $variable_submited_options = customizable_entities_variable_datas($entity_type, $entity, 'submitted', 'get');

  if ($variable_submited_options && ($has_author || $has_date)) {
    $variables['display_submitted'] = TRUE;
    if ($has_author && $has_date) {
      $variables['submitted'] = t('Submitted by !username on !datetime', array('!username' => $variables['name'], '!datetime' => $variables['date']));
    }
    elseif ($has_author) {
      $variables['submitted'] = t('Submitted by !username', array('!username' => $variables['name']));
    }
    else { // $has_date
      $variables['submitted'] = t('Submitted on !datetime', array('!datetime' => $variables['date']));
    }
    $variables['user_picture'] = (theme_get_setting('toggle_node_user_picture') && isset($account)) ? theme('user_picture', array('account' => $account)) : '';
  }
  else {
    $variables['display_submitted'] = FALSE;
    $variables['submitted'] = '';
    $variables['user_picture'] = '';
  }

  // Gather node classes.
  if (!empty($info['entity keys']['bundle'])) {
    $variables['classes_array'][] = drupal_html_class('customizable_entities-' . $entity->{$info['entity keys']['bundle']});
  }
  $variables['promote'] = (in_array('promote', $info['schema_fields_sql']['base table'])) ? $entity->promote : NULL;
  $variables['sticky'] = (in_array('sticky', $info['schema_fields_sql']['base table'])) ? $entity->sticky : NULL;
  $variables['status'] = (in_array('status', $info['schema_fields_sql']['base table'])) ? $entity->status : TRUE; // IF not published property, set as published.
  if ($variables['promote']) {
    $variables['classes_array'][] = 'node-promoted';
  }
  if ($variables['sticky']) {
    $variables['classes_array'][] = 'node-sticky';
  }
  if (!$variables['status']) {
    $variables['classes_array'][] = 'node-unpublished';
  }
  if ($variables['teaser']) {
    $variables['classes_array'][] = 'node-teaser';
  }
  if (isset($variables['preview'])) {
    $variables['classes_array'][] = 'node-preview';
  }
}

// fork of the theme_node_preview() function
function theme_customizable_entities_preview($variables) {
  $entity = $variables['entity'];
  $entity_type = $variables['entity_type'];

  $output = '<div class="preview">';

  $preview_trimmed_version = FALSE;

  $elements = entity_view($entity_type, array(clone $entity), 'teaser');
  $trimmed = drupal_render($elements);
  $elements = entity_view($entity_type, array($entity), 'full');
  $full = drupal_render($elements);

  // Do we need to preview trimmed version of post as well as full version?
  if ($trimmed != $full) {
    drupal_set_message(t('The trimmed version of your post shows what your post looks like when promoted to the main page or when exported for syndication.<span class="no-js"> You can insert the delimiter "&lt;!--break--&gt;" (without the quotes) to fine-tune where your post gets split.</span>'));
    $output .= '<h3>' . t('Preview trimmed version') . '</h3>';
    $output .= $trimmed;
    $output .= '<h3>' . t('Preview full version') . '</h3>';
    $output .= $full;
  }
  else {
    $output .= $full;
  }
  $output .= "</div>\n";

  return $output;
}

// fork of the theme_node_add_list() function
function theme_customizable_entities_add_list($variables) {
  $entity_type = $variables['entity_type'];
  $content = $variables['content'];
  $output = '';

  if ($content) {
    $output = '<dl class="node-type-list">';
    foreach ($content as $item) {
      $output .= '<dt>' . l($item['title'], $item['href'], $item['localized_options']) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
    }
    $output .= '</dl>';
  }
  else {
    $add_path = FALSE;
    if ($bundle_type = customizable_entities_entity_find_bundle_entity_type($entity_type)) {
      if ($bundle_info = entity_get_info($bundle_type)) {
        if (!empty($bundle_info['customizable entities']['crud path'])) {
          $add_path = $bundle_info['customizable entities']['crud path'];
        }
      }
    }
    if ($add_path) {
      $output = '<p>' . t('You have not created any types yet. Go to the <a href="@create-content">type creation page</a> to add a new type.', array('@create-content' => url($add_path))) . '</p>';
    }
    else {
      $output = '<p>' . t('You have not created any types yet. Go to the type creation page to add a new type.') . '</p>';
    }
  }
  return $output;
}
