<?php

/**
 * The entity info class for customized entities from UI.
 */

class CustomizableEntitiesEntityInfoCustomizableEntitiesBuilderBuilded extends CustomizableEntitiesEntityInfo {
  // Set new variables for retrieving parameters

  public function __construct($mymodule = NULL, $source = NULL, $parent_class = NULL, $source_class = NULL) {
    parent::__construct($mymodule, $source, $parent_class, $source_class);
  }
  /*
   * Populate class config datas from the source, the builder entity.
   */
  protected function setup($bundle) {
    parent::setup($bundle);
    // We populate the entity_info parameters from the builder entity.
    $this->entityname = $bundle->name;
    $this->label = $bundle->label;
    if (!empty($bundle->data['bundle_of'])) {
      $this->bundle_of = $bundle->data['bundle_of'];
    }
    // Populate all parameters from the form builder.
    foreach (customizable_entities_builder_custom_col() as $fieldset_name => $fieldset_properties) {
      foreach ($fieldset_properties['fields'] as $col => $properties) {
        if (isset($bundle->data[$col])) {
          $this->$col = $bundle->data[$col];
        }
      }
    }
  }
}

class CustomizableEntitiesEntityInfoCustomizableEntitiesBuilderBuildedSubentities extends CustomizableEntitiesEntityInfoSubentities {
  // Set new variables for retrieving parameters
  protected $source_class = 'CustomizableEntitiesEntityInfoCustomizableEntitiesBuilder';
  protected $info_class = 'CustomizableEntitiesEntityInfoCustomizableEntitiesBuilderBuilded';
}
