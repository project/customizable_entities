<?php

/**
 * The entity info class for esan_config entities.
 */
class CustomizableEntitiesEntityInfoCustomizableEntitiesBuilder extends CustomizableEntitiesEntityInfo {

  // Entity machine name : no spaces.
  protected $entityname = 'customizable_entities_builder';
  protected $label = 'Customizable entities builder';
  // Default plural label.
  protected $plural_label = 'Customizable entities builders';

  protected $name_col = TRUE; // will store the entity_type
  protected $title_col = TRUE; // will store the entity label
  protected $title_col_name = 'label'; // the title colomn name.
  protected $data_col = TRUE; // will store options (colomns, etc...)

  protected $overview_path = 'admin/structure/customizable-entities/customizable-entities-builder';
  protected $crud_path = 'admin/structure/customizable-entities/customizable-entities-builder';
  //protected $root_admin_ui = 'admin/structure/customizable-entities/customizable_entities_builder';

  protected $view = FALSE; // no view page

  // Optional :
  protected $revision = TRUE; // We set it as revisionnable.
  protected $description_col = TRUE;
}
