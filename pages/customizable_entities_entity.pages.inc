<?php

/**
 * return entity_view result, if the entity is provided as an object either as
 * primary numeric key.
 */
function customizable_entities_entity_view_page($entity_type, $entity, $view_mode = 'full', $langcode = NULL, $page = NULL) {

  // This function exists because the entity object must be keyed.
  $entities = array($entity);
  if (!is_object($entity) && !empty($entity)) {
    $entities = entity_load($entity_type, $entities);
  }
  return entity_view($entity_type, $entities, $view_mode, $langcode, $page);
}

// Fork of the entity_ui_get_form() function, add possibiity to add entity with bundle and change the wrapper for adding buton and options, as author, etc...
// Called from class:hook_menu()
function customizable_entities_entity_entity_ui_get_form($entity_type, $entity, $op = 'edit', $form_state = array(), $bundle_provided = NULL) {

  $entity_info = entity_get_info($entity_type);
  if (isset($entity)) {
    list(, , $bundle) = entity_extract_ids($entity_type, $entity);
  }

  $form_id = (!isset($bundle) || $bundle == $entity_type) ? $entity_type . '_form' : $entity_type . '_edit_' . $bundle . '_form';

  // Here need to create the entity. The bundle may be required.
  // Try to guess it or use the one provided.
  if (!isset($entity) && $op == 'add') {
    // Handle the bundle parameter.
    $bundle_parameter_needed = !empty($entity_info['entity keys']['bundle']);
    $only_1_bundle = ($bundle_parameter_needed && count($entity_info['bundles']) == 1 && !empty($entity_info['bundles'])) ? key($entity_info['bundles']) : FALSE;
    // Find the Bundle entity type.
    $bundle_entity_type = customizable_entities_entity_find_bundle_entity_type($entity_type);
    $only_1_bundle_valid = ($only_1_bundle && !empty(entity_load_multiple_by_name($bundle_entity_type, array($only_1_bundle))));
    $many_bundles = ($bundle_parameter_needed && count($entity_info['bundles']) > 1);
    // No act depending on the situation.
    $values = array();
    if ($only_1_bundle) {
      if ($only_1_bundle_valid) {
        $values = array($entity_info['entity keys']['bundle'] => $only_1_bundle);
      }
      else {
        drupal_not_found();
        exit;
      }
    }
    elseif ($many_bundles) {
      if (!empty($bundle_provided) && !empty(entity_load_multiple_by_name($bundle_entity_type, array($bundle_provided)))) {
        $values = array($entity_info['entity keys']['bundle'] => $bundle_provided);

      }
      else {
        drupal_not_found();
        exit;
      }
    }

    $entity = entity_create($entity_type, $values);
  }

  // Do not use drupal_get_form(), but invoke drupal_build_form() ourself so
  // we can prepulate the form state.
  //$form_state['wrapper_callback'] = 'entity_ui_main_form_defaults';
  // HERE : I change for my wrapper callback
  $form_state['wrapper_callback'] = 'customizable_entities_entity_entity_ui_main_form_defaults';
  $form_state['entity_type'] = $entity_type;
  form_load_include($form_state, 'inc', 'entity', 'includes/entity.ui');
  // Load customizable_entities fonctions.
  form_load_include($form_state, 'inc', 'customizable_entities', 'pages/customizable_entities_entity.pages');

  // Handle cloning. We cannot do that in the wrapper callback as it is too late
  // for changing arguments.
  if ($op == 'clone') {
    $entity = entity_ui_clone_entity($entity_type, $entity);
    // Add ' cloned' to the title.
    if (!empty($entity_info['entity keys']['label'])) {
      $entity->{$entity_info['entity keys']['label']} .= ' (cloned)';
    }
  }

  // We don't pass the entity type as first parameter, as the implementing
  // module knows the type anyway. However, in order to allow for efficient
  // hook_forms() implementiations we append the entity type as last argument,
  // which the module implementing the form constructor may safely ignore.
  // @see entity_forms()
  $form_state['build_info']['args'] = array($entity, $op, $entity_type);
  return drupal_build_form($form_id, $form_state);
}

// Rewrite entity_ui_main_form_defaults() adding more elements
function customizable_entities_entity_entity_ui_main_form_defaults($form, &$form_state, $entity = NULL, $op = NULL) {
  // Now equals entity_ui_form_defaults() but is still here to keep backward
  // compatibility.
  $form = entity_ui_form_defaults($form, $form_state, $form_state['entity_type'], $entity, $op);
  // The entity API has added its stuf, my turn.
  // Delete entity_ui validatation and submit fonctions from entity_ui_form_defaults()
  //unset($form['#validate'], $form['#submit']);
  // Delete entity API validations and submits. Use my own instead.
  if (!empty($form['#validate']) && ($key_validate = array_search('entity_ui_controller_form_validate', $form['#validate'])) !== FALSE) {
    unset($form['#validate'][$key_validate]);
  }
  if (!empty($form['#submit']) && ($key_submit = array_search('entity_ui_controller_form_submit', $form['#submit'])) !== FALSE) {
    unset($form['#submit'][$key_submit]);
  }

  $entity_type = $form_state['entity_type'];
  // Prepare entity regarding revison, author, published

  // Add buttons
  $op = $form_state['op'];
  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => empty($entity->is_new) ? t('Update') : t('Save'),
    '#weight' => 40,
    // Save only on the 'save' button. Preview needs this.
    '#submit' => array('customizable_entities_entity_edit_form_submit'),
  );
  $form['#validate'] = array('customizable_entities_entity_edit_form_validate');


  // Add custom validate function if exists.
  $form_ids = array($form_state['build_info']['form_id']);
  if (!empty($form_state['build_info']['base_form_id'])) {
    array_unshift($form_ids, $form_state['build_info']['base_form_id']);
  }
  foreach ($form_ids as $form_id) {
    $item_id = $form_id . '_validate';
    if (function_exists($item_id)) {
      // execute before entity_save
      array_unshift($form['#validate'], $item_id);
    }
    $item_id = $form_id . '_submit';
    if (function_exists($item_id)) {
      // execute before entity_save
      array_unshift($form['actions']['submit']['#submit'], $item_id);
    }
  }

  if ($op != 'add' && $op != 'clone') { // !$entity->isLocked() &&
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('customizable_entities_entity_delete_form_submit'),
      '#access' => entity_access('delete', $entity_type, $entity),
    );
  }

  // add stuff for revison, author, published,
  customizable_entities_entity_edit_form($form, $form_state, $entity, $entity_type);

  // Load the entity_reference field (and maybe more custonm fields).
  field_attach_form($entity_type, $entity, $form, $form_state);

  return $form;
}

/**
 * Inspired by the node_form() core function
 */
function customizable_entities_entity_edit_form(&$form, $form_state, $entity, $entity_type) {
  $entity_info = entity_get_info($entity_type);
  if (empty($entity_info)) {
    return array();
  }

  if (empty($form)) {
    $form = array();
  }

  // Set a flag for the controller:save() method, informing that this entity was
  // built from the UI. Thanks to this flag, display succes message will be
  // fired from the controller.
  $form['from_ui'] = array(
    '#type' => 'value',
    '#value' => TRUE,
  );

  // Some special stuff when previewing a node.
  if (isset($form_state['customizable_entities_preview'])) {
    $form['#prefix'] = $form_state['customizable_entities_preview'];
    $entity->in_preview = TRUE;
  }
  else {
    unset($entity->in_preview);
  }

  // Set default values from context to entity
  customizable_entities_entity_prepare($entity_type, $entity);

  // Check if this entity own a bundle
  $has_bundle = (!empty($entity_info['entity keys']['bundle']));
  if ($has_bundle) {
    $bundle_entity_type = customizable_entities_entity_find_bundle_entity_type($entity_type);
    $bundle_entities = entity_load_multiple_by_name($bundle_entity_type, array($entity->{$entity_info['entity keys']['bundle']}));
    $bundle_entity = (!empty($bundle_entities) && count($bundle_entities) == 1) ? current($bundle_entities) : FALSE;
    $bundle_entity_info = entity_get_info($bundle_entity_type);
    $bundle_title = $bundle_entity->{$bundle_entity_info['entity keys']['label']};
    $title_label = (in_array('title_label', $bundle_entity_info['schema_fields_sql']['base table'])) ? $bundle_entity->title_label : t('Title');
  }
  else {
    $bundle_title = (!empty($entity_info['entity keys']['label']) && isset($entity->{$entity_info['entity keys']['label']})) ? $entity->{$entity_info['entity keys']['label']} : $entity_info['label'];
    $title_label = t('Title');
  }

  // Basic entity information.
  // These elements are just values so they are not even sent to the client.
  // Prepare datas. They may be overwritten later.
  $set_as_value = array();
  if (!empty($entity_info['entity keys']['id'])) {
    $set_as_value[] = $entity_info['entity keys']['id'];
  }
  if (!empty($entity_info['entity keys']['bundle'])) {
    $set_as_value[] = $entity_info['entity keys']['bundle'];
  }
  foreach ($set_as_value as $entity_key) {
    $form[$entity_key] = array(
      '#type' => 'value',
      '#value' => isset($entity->$entity_key) ? $entity->$entity_key : NULL,
    );
  }

  if (in_array('created', $entity_info['schema_fields_sql']['base table'])) {
    // Changed must be sent to the client, for later overwrite error checking.
    $form['changed'] = array(
      '#type' => 'hidden',
      '#default_value' => isset($entity->changed) ? $entity->changed : NULL,
    );
  }

  // If the node type has a title, and the node type form defined no special
  // weight for it, we default to a weight of -5 for consistency.
  if (!empty($entity_info['entity keys']['label'])) {
    $title_field = $entity_info['entity keys']['label'];
    if (!isset($form[$title_field])) {
      // add title edit form
      // from node_content_form()
      // Title from the bundle
      $form[$title_field] = array(
        '#type' => 'textfield',
        '#title' => check_plain($title_label),
        '#required' => TRUE,
        '#default_value' => $entity->$title_field,
        '#maxlength' => 255,
        '#weight' => -5,
      );

    }
  }

  if (in_array('name', $entity_info['schema_fields_sql']['base table'])) { // && !empty($entity_info['bundle of'])) {
    $form['name'] = array(
      '#type' => 'machine_name',
      '#default_value' => $entity->name,
      '#maxlength' => 32,
      //'#disabled' => $entity->locked,
      '#required' => TRUE,
      '#machine_name' => array(
        'exists' => 'customizable_entities_bundle_name_exists',
      ),
      '#weight' => -4,
      '#description' => t('A unique machine-readable name for this entity type. It must only contain lowercase letters, numbers, and underscores. This name will be used for constructing the URL of the %entity-add page, in which underscores will be converted into hyphens.', array(
        '%entity-add' => t('Add new @type', array('@type' => $bundle_title)),
      )),
    );
    if (!empty($entity_info['entity keys']['label']) && !empty($form[$entity_info['entity keys']['label']])) {
      $form['name']['#machine_name']['source'] = array($entity_info['entity keys']['label']);
    }
  }

  if (in_array('description', $entity_info['schema_fields_sql']['base table'])) {
    $form['description'] = array(
      '#type' => 'textarea', // or 'text_format' if we want formated text
      '#title' => t('Description'),
      '#default_value' => $entity->description,
      //'#format' => $entity->format, // Below.
      '#weight' => 0,
    );
    // set format
    global $user;
    $formats = filter_formats($user);
    // @TODO : 'filtered_html' and 'plain_text' may not exist (deleted).
    $format = (isset($formats['filtered_html'])) ? 'filtered_html' : ((isset($formats['plain_text'])) ? 'plain_text' : FALSE);
    if ($format) {
      //$form['description']['#format'] = $format;
    }
  }
  if (in_array('weight', $entity_info['schema_fields_sql']['base table'])) {
    $form['weight'] = array(
      '#type' => 'textfield',
      '#title' => t('Weight'),
      '#size' => 6,
      '#default_value' => $entity->weight,
      '#description' => t('@entities are displayed in ascending order by weight.', array('@entities' => $entity_info['plural label'])),
      '#required' => TRUE,
    );
  }

  if (in_array('language', $entity_info['schema_fields_sql']['base table'])) {
    // Set the Drupal language
    if (!drupal_multilingual() || !module_exists('locale')) {
      // Only one language, don't display it.
      $form['language'] = array(
        '#type' => 'value',
        '#value' => $entity->language,
      );
    }
    else {
      $languages = language_list('enabled');
      $languages = $languages[1];
      $names = array();
      foreach ($languages as $langcode => $item) {
        $names[$langcode] = t($item->name);
      }
      $form['language'] = array(
        '#type' => 'select',
        '#title' => t('Select a displayed language'),
        '#options' => $names,
        '#default_value' => $entity->language,
      );
    }
  }

  $form['additional_settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 99,
  );

  $perm_admin = customizable_entities_permission_item('administer', $entity_type);

  // Add a log field if the "Create new revision" option is checked, or if the
  // current user has the ability to check that option.
  if (!empty($entity_info['entity keys']['revision'])) {
    if (!isset($entity->revision)) {
      // @TODO : create an admin interface like node_type_form().
      $entity->revision = 1;
    }
    $form['revision_information'] = array(
      '#type' => 'fieldset',
      '#title' => t('Revision information'),
      '#collapsible' => TRUE,
      // Collapsed by default when "Create new revision" is unchecked
      '#collapsed' => !$entity->revision,
      '#group' => 'additional_settings',
      '#attributes' => array(
        'class' => array('node-form-revision-information'),
      ),
      '#attached' => array(
        // Fork of the node.js core file, change name to username.
        'js' => array(drupal_get_path('module', 'customizable_entities') . '/js/customizable_entities_entity.js'),
        //'js' => array(drupal_get_path('module', 'node') . '/node.js'),
      ),
      '#weight' => 20,
      '#access' => $entity->revision || user_access($perm_admin),
    );
    $form['revision_information']['revision'] = array(
      '#type' => 'checkbox',
      '#title' => t('Create new revision'),
      '#default_value' => $entity->revision,
      '#access' => user_access($perm_admin),
    );
    // Check the revision log checkbox when the log textarea is filled in.
    // This must not happen if "Create new revision" is enabled by default, since
    // the state would auto-disable the checkbox otherwise.
    if (!$entity->revision) {
      $form['revision_information']['revision']['#states'] = array(
        'checked' => array(
          'textarea[name="log"]' => array('empty' => FALSE),
        ),
      );
    }
    // Remove the log message from the original node object.
    $entity->log = NULL;
    $form['revision_information']['log'] = array(
      '#type' => 'textarea',
      '#title' => t('Revision log message'),
      '#rows' => 4,
      '#default_value' => !empty($form_state['values']['log']) ? $form_state['values']['log'] : (!empty($entity->log) ? $entity->log : ''),
      '#description' => t('Provide an explanation of the changes you are making. This will help other authors understand your motivations.'),
    );
  }

  // Node author information for administrators
  if (in_array('uid', $entity_info['schema_fields_sql']['base table'])) {
    $form['author'] = array(
      '#type' => 'fieldset',
      '#access' => user_access($perm_admin),
      '#title' => t('Authoring information'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'additional_settings',
      '#attributes' => array(
        'class' => array('node-form-author'),
      ),
      '#attached' => array(
        'js' => array(
          // Fork of the node.js core file, change name to username.
          drupal_get_path('module', 'customizable_entities') . '/js/customizable_entities_entity.js',
          array(
            'type' => 'setting',
            'data' => array('anonymous' => variable_get('anonymous', t('Anonymous'))),
          ),
        ),
      ),
      '#weight' => 90,
    );

    $form['author']['username'] = array(
      '#type' => 'textfield',
      '#title' => t('Authored by'),
      '#maxlength' => 60,
      '#autocomplete_path' => 'user/autocomplete',
      '#default_value' => !empty($entity->username) ? $entity->username : '',
      '#weight' => -1,
      '#description' => t('Leave blank for %anonymous.', array('%anonymous' => variable_get('anonymous', t('Anonymous')))),
    );

    // Date information for administrators
    if (in_array('created', $entity_info['schema_fields_sql']['base table'])) {
      // $entity->date is set in customizable_entities_entity_prepare().
      $form['author']['date'] = array(
        '#type' => 'textfield',
        '#title' => t('Authored on'),
        '#maxlength' => 25,
        '#description' => t('Format: %time. The date format is YYYY-MM-DD and %timezone is the time zone offset from UTC. Leave blank to use the time of form submission.', array('%time' => !empty($entity->date) ? date_format(date_create($entity->date), 'Y-m-d H:i:s O') : format_date($entity->created, 'custom', 'Y-m-d H:i:s O'), '%timezone' => !empty($entity->date) ? date_format(date_create($entity->date), 'O') : format_date($entity->created, 'custom', 'O'))),
        '#default_value' => !empty($entity->date) ? $entity->date : '',
      );
    }
  }

  $status_col = in_array('status', $entity_info['schema_fields_sql']['base table']);
  $promote_col = in_array('promote', $entity_info['schema_fields_sql']['base table']);
  $sticky_col = in_array('sticky', $entity_info['schema_fields_sql']['base table']);
  if ($status_col || $promote_col || $sticky_col) {
    // Values provided into the customizable_entities_entity_prepare() function.
    // Node options for administrators
    $form['options'] = array(
      '#type' => 'fieldset',
      '#access' => user_access($perm_admin),
      '#title' => t('Publishing options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'additional_settings',
      '#attributes' => array(
        'class' => array('node-form-options'),
      ),
      '#attached' => array(
        // Fork of the node.js core file, change name to username.
        'js' => array(drupal_get_path('module', 'customizable_entities') . '/js/customizable_entities_entity.js'),
        //'js' => array(drupal_get_path('module', 'node') . '/node.js'),
      ),
      '#weight' => 95,
    );
    if ($status_col) {
      $form['options']['status'] = array(
        '#type' => 'checkbox',
        '#title' => t('Published'),
        '#default_value' => $entity->status,
      );
    }
    if ($promote_col) {
      $form['options']['promote'] = array(
        '#type' => 'checkbox',
        '#title' => t('Promoted to front page'),
        '#default_value' => $entity->promote,
      );
    }
    if ($sticky_col) {
      $form['options']['sticky'] = array(
        '#type' => 'checkbox',
        '#title' => t('Sticky at top of lists'),
        '#default_value' => $entity->sticky,
      );
    }
  }

  $variable_preview = customizable_entities_variable_datas($entity_type, $entity, 'preview', 'get');
  // Hide the save button depending on the preview options
  if ($variable_preview == DRUPAL_REQUIRED) {
    $form['actions']['submit']['#access'] = $variable_preview != DRUPAL_REQUIRED || (!form_get_errors() && isset($form_state['customizable_entities_preview']));
  }
  $form['actions']['preview'] = array(
    '#access' => $variable_preview != DRUPAL_DISABLED,
    '#type' => 'submit',
    '#value' => t('Preview'),
    '#weight' => 10,
    '#submit' => array('customizable_entities_form_build_preview'),
  );

  // the edited entity is a bundle for an other entity, add default options to the new subentity
  if (!empty($entity_info['bundle of'])) {
    // Submit function for entity_datas will be execute after the entity save
    // function.
    customizable_entities_entity_default_options_form($form, $form_state, $entity_info['bundle of'], $entity);
  }

  return $form;
}

// Inspired from node_form_validate();
function customizable_entities_entity_edit_form_validate($form, &$form_state) {
  $entity_type = $form_state['entity_type'];
  //$orig_entity = $form_state[$entity_type];
  // From node_form_validate()
  // $form_state['node'] contains the actual entity being edited, but we must
  // not update it with form values that have not yet been validated, so we
  // create a pseudo-entity to use during validation.
  $pseudo_entity = (object) $form_state['values'];
  // Fix the uid =>
  // @TODO
  customizable_entities_entity_validate($pseudo_entity, $form, $form_state);
  entity_form_field_validate($entity_type, $form, $form_state);
}

// Inspired from node_validate();
function customizable_entities_entity_validate($entity, $form, $form_state) {
  $entity_type = $form_state['entity_type'];
  $entity_info = entity_get_info($entity_type);
  $id_field = $entity_info['entity keys']['id'];
  if (isset($entity->date) && in_array('changed', $entity_info['schema_fields_sql']['base table'])) {
    if (isset($entity->{$id_field}) && (customizable_entities_entity_last_changed($entity_type, $entity) > $entity->date)) {
      form_set_error('date', t('The content on this page has either been modified by another user, or you have already submitted modifications using this form. As a result, your changes cannot be saved.'));
    }
  }

  if (isset($entity->username) && in_array('uid', $entity_info['schema_fields_sql']['base table'])) {
    // Validate the "authored by" field.
    $name = $entity->username;
    if (!empty($name) && !($account = user_load_by_name($name))) {
      // The use of empty() is mandatory in the context of usernames
      // as the empty string denotes the anonymous user. In case we
      // are dealing with an anonymous user we set the user ID to 0.
      form_set_error('username', t('The username %name does not exist.', array('%name' => $name)));
    }
  }

  if (isset($entity->date) && in_array('changed', $entity_info['schema_fields_sql']['base table'])) {
    // Validate the "authored on" field.
    $date = $entity->date;
    if (!empty($date) && strtotime($date) === FALSE) {
      form_set_error('date', t('You have to specify a valid date.'));
    }
  }

  // The machine name must be unique
  if (!empty($entity_info['entity keys']['name'])) {
    $name = $entity->{$entity_info['entity keys']['name']};
    $entities_name = entity_load_multiple_by_name($entity_type, array($name));
    if (!empty($entities_name) && $entity->{$entity_info['entity keys']['id']} != $entities_name[$name]->{$entity_info['entity keys']['id']}) {
      form_set_error($entity_info['entity keys']['name'], t("Invalid machine-readable name. Enter a name other than %invalid.", array('%invalid' => $name)));
    }
  }
}

/**
 * Finds the last time an entity was changed.
 * From node_last_changed().
 *
 * @param $nid
 *   The ID of a node.
 *
 * @return
 *   A unix timestamp indicating the last time the entity was changed or FALSE.
 */
function customizable_entities_entity_last_changed($entity_type, $entity) {
  $entity_info = entity_get_info($entity_type);
  $col_changed = 'changed';
  if ($entity_info && is_object($entity) && in_array($col_changed, $entity_info['schema_fields_sql']['base table']) && !empty($entity_info['entity keys']['id'])) {
    $id_field = $entity_info['entity keys']['id'];
    $id = $entity->{$id_field};
    if (!empty($id)) {
      return db_query('SELECT ' . $col_changed . ' FROM {' . $entity_info['base table'] . '} WHERE ' . $id_field . ' = :id', array(':id' => $id))->fetch()->{$col_changed};
    }
  }
  return FALSE;
}

// Inspired from node_form_submit();
function customizable_entities_entity_edit_form_submit($form, &$form_state) {
  // @todo Legacy support for modules that extend the node form with form-level
//   submit handlers that adjust $form_state['values'] prior to those values
//   being used to update the entity. Module authors are encouraged to instead
//   adjust the node directly within a hook_node_submit() implementation. For
//   Drupal 8, evaluate whether the pattern of triggering form-level submit
//   handlers during button-level submit processing is worth supporting
//   properly, and if so, add a Form API function for doing so.
  // Not sure below is needed...
  //unset($form_state['submit_handlers']);
  //form_execute_handlers('submit', $form, $form_state);

  $entity_type = $form_state['entity_type'];
  $entity_info = entity_get_info($entity_type);
  $id_field = $entity_info['entity keys']['id'];
  // retreive the original entity.
  $entity = $form_state[$entity_type];
  $insert = empty($entity->{$id_field});
  // apply values form form and fields forms to $entity.
  entity_form_submit_build_entity($entity_type, $entity, $form, $form_state);
  // Delete remaining garbage datas from entity_ui_form_submit_build_entity().
  $garbage = array(
    'submit',
    'form_build_id',
    'form_token',
    'form_id',
    'op',
    'delete',
  );

  foreach ($garbage as $garbage_field) {
    if (isset($entity->$garbage_field)) {
      unset($entity->$garbage_field);
    }
  }

  // Specific changes on node (author, date).
  customizable_entities_entity_edit_submit($entity, $entity_type);

  entity_save($entity_type, $entity);

  // watchdog is handled into the controller class save method
  // Display message is handled into the controller class save method.
  if ($entity->{$id_field}) {
    $form_state['values'][$id_field] = $entity->{$id_field};
    $form_state[$id_field] = $entity->{$id_field};
    $form_state['redirect'] = entity_access('view', $entity_type, $entity) ? current(entity_uri($entity_type, $entity)) : '<front>';
  }
  else {
    // In the unlikely case something went wrong on save, the node will be
    // rebuilt and node form redisplayed the same way as in preview.
    drupal_set_message(t('The data could not be saved.'), 'error');
    $form_state['rebuild'] = TRUE;
  }

  // Clear the page and block caches. --> done in controller.
  // cache_clear_all();
}

function customizable_entities_bundle_name_exists($url_name, $entity) {
  $name = strtr($url_name, array('-' => '_'));
  $entity_type = (!empty($entity->entity_type)) ? $entity->entity_type : FALSE;
  if ($entity_type && $name && is_string($name)) {
    $entities = entity_load_multiple_by_name($entity_type, array($name));
    if (!empty($entities)) {
      return TRUE;
    }
  }
  return FALSE;
}

// Inspired from node_submit();
function customizable_entities_entity_edit_submit($entity, $entity_type = NULL) {
  $entity_type = (!empty($entity_type)) ? $entity_type : $entity->entity_type;
  $entity_info = entity_get_info($entity_type);

  if (in_array('uid', $entity_info['schema_fields_sql']['base table'])) {
    // A user might assign the node author by entering a user name in the node
    // form, which we then need to translate to a user ID.
    if (isset($entity->username)) {
      if ($account = user_load_by_name($entity->username)) {
        $entity->uid = $account->uid;
      }
      else {
        $entity->uid = 0;
      }
    }
  }

  if (in_array('created', $entity_info['schema_fields_sql']['base table'])) {
    $entity->created = !empty($entity->date) ? strtotime($entity->date) : REQUEST_TIME;
    $entity->validated = TRUE;
  }
  return $entity;
}

// Button delete redirect to delete page
function customizable_entities_entity_delete_form_submit($form, &$form_state) {
  $entity_type = $form_state['entity_type'];
  $entity = $form_state[$entity_type];
  $entity_path = current(entity_uri($entity_type, $entity));
  drupal_goto($entity_path . '/delete');
}

function customizable_entities_entity_default_options_form(&$form, $form_state, $entity_type, $bundle = NULL) {
  // Display form for bundle entity or general settings on a unbundle entity.
  // Check if this entity own a bundle
  $entity_info = entity_get_info($entity_type);
  if (empty($entity_info)) {
    return;
  }
  $default_bundle = (empty($entity_info['entity keys']['bundle']));
  if ($default_bundle) {
    $bundle_name = $entity_type;
  }
  elseif (!$default_bundle && !empty($bundle)) {
    $bundle_entity_type = customizable_entities_entity_find_bundle_entity_type($entity_type);
    $bundle_entity_info = entity_get_info($bundle_entity_type);
    if (!empty($bundle_entity_type) && !empty($bundle_entity_info)) {
      if (is_string($bundle)) {
        $bundle_object = ($bundle_entity_type) ? entity_load_single($bundle_entity_type, $bundle) : FALSE;
      }
      elseif (is_object($bundle)) {
        $bundle_object = $bundle;
      }
      if (!empty($bundle_object) && !empty($entity_info['bundle keys']['bundle'])) {
        $bundle_name = (!empty($bundle_object->{$entity_info['bundle keys']['bundle']})) ? $bundle_object->{$entity_info['bundle keys']['bundle']} : FALSE;
      }
    }
  }

  if (empty($bundle_name) || (!$default_bundle && empty($bundle_object))) {
    // if the bundle is needed and is not available, stop here.
    //return FALSE; // For new bundle creation, Bundle name is not set.
    // else : if !$default_bundle the following variables are availables :
    // $bundle_name, $bundle_object, $bundle_entity_type, $bundle_entity_info.
  }

  //Make sure the field_set exists
  if (!isset($form['additional_settings'])) {
    $form['additional_settings'] = array(
      '#type' => 'vertical_tabs',
      '#weight' => 99,
    );
  }
  // Make sure the js is attached.
  $js_content_type = drupal_get_path('module', 'customizable_entities') . '/js/customizable_entities_bundle.js';
  if (!isset($form['additional_settings']['#attached']['js']) || !in_array($js_content_type, $form['additional_settings']['#attached']['js'])) {
    $form['additional_settings']['#attached']['js'][] = $js_content_type;
  }

  // retrieve variables informations
  $variable_options = customizable_entities_variable_datas($entity_type, $bundle_name, 'options', 'get');
  $variable_preview = customizable_entities_variable_datas($entity_type, $bundle_name, 'preview', 'get');
  $variable_submitted = customizable_entities_variable_datas($entity_type, $bundle_name, 'submitted', 'get');

  // bellow from node_type_form
  $form['submission'] = array(
    '#type' => 'fieldset',
    '#title' => t('Submission form settings'),
    '#collapsible' => TRUE,
    '#group' => 'additional_settings',
  );
  if (!$default_bundle && in_array('title_label', $bundle_entity_info['schema_fields_sql']['base table'])) {
    $form['submission']['title_label'] = array(
      '#title' => t('Title field label'),
      '#type' => 'textfield',
      '#default_value' => $bundle_object->title_label,
      '#required' => TRUE,
    );
  }

  $form['submission']['customizable_entities_preview'] = array(
    '#type' => 'radios',
    '#title' => t('Preview before submitting'),
    '#default_value' => $variable_preview,
    '#options' => array(
      DRUPAL_DISABLED => t('Disabled'), // 0
      DRUPAL_OPTIONAL => t('Optional'), // 1
      DRUPAL_REQUIRED => t('Required'), // 2
    ),
  );

  if (!$default_bundle && in_array('help', $bundle_entity_info['schema_fields_sql']['base table'])) {
    $form['submission']['help'] = array(
      '#type' => 'textarea',
      '#title' => t('Explanation or submission guidelines'),
      '#default_value' => $bundle_object->help,
      '#description' => t('This text will be displayed at the top of the page when creating or editing content of this type.'),
    );
  }

  $publishing_col = array('status', 'promote', 'sticky');
  if (!empty($entity_info['entity keys']['revision'])) {
    $publishing_col[] = 'revision';
  }
  if (count(array_intersect($entity_info['schema_fields_sql']['base table'], $publishing_col)) > 0) {
    // At least one of publishing col exists is the entity
    $all_options = array(
      'status' => t('Published'),
      'promote' => t('Promoted to front page'),
      'sticky' => t('Sticky at top of lists'),
      'revision' => t('Create new revision'),
    );
    $current_options = array();
    foreach ($all_options as $field => $label) {
      if (
        ($field != 'revision' && in_array($field, $entity_info['schema_fields_sql']['base table']))
         || ($field == 'revision' && !empty($entity_info['entity keys']['revision']))
      ) {
        $current_options[$field] = $all_options[$field];
      }
    }
    $form['workflow'] = array(
      '#type' => 'fieldset',
      '#title' => t('Default publishing options'), // Add 'default'.
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'additional_settings',
    );
    $form['workflow']['customizable_entities_options'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Default options'),
      '#default_value' => $variable_options,
      '#options' => $current_options,
      '#description' => t('Users with the <em>Administer @entity</em> permission will be able to override these options.', array('@entity' => $entity_info['label'])),
    );
  }

  $display_col = array('created', 'uid');
  $display_match = array_intersect($entity_info['schema_fields_sql']['base table'], $display_col);
  if (count($display_match) > 0) {
    if (count($display_match) == 2) {
      $display_title = t('Display author and date information.');
      $display_description = t('Author username and publish date will be displayed.');
    }
    elseif (in_array('uid', $display_match)) {
      $display_title = t('Display author information.');
      $display_description = t('Author username will be displayed.');
    }
    else {
      $display_title = t('Display date information.');
      $display_description = t('Publish date will be displayed.');
    }
    $form['display'] = array(
      '#type' => 'fieldset',
      '#title' => t('Display settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'additional_settings',
    );

    $form['display']['customizable_entities_submitted'] = array(
      '#type' => 'checkbox',
      '#title' => $display_title,
      '#default_value' => $variable_submitted,
      '#description' => $display_description,
    );
  }

  // On editing a bundle entity it will be saved into the contoller.
  if ($default_bundle) {
    // Add the submit only to the save button, in order to not execute it on preview.
    $form['actions']['submit']['#submit'][] = 'customizable_entities_entity_default_options_form_submit';
  }
}

function customizable_entities_entity_default_options_form_submit($form, $form_state) {
  // Save the variables values
  // from node_type_form_submit()
  $entity_type = $form_state['entity_type'];
  $entity_info = entity_get_info($entity_type);
  if (empty($entity_info)) {
    return FALSE;
  }
  $changed_config = customizable_entities_entity_options_save($entity_type, $form_state['values']);
}

// fork of the node_form_build_preview() function
function customizable_entities_form_build_preview($form, &$form_state) {
  // @todo Legacy support for modules that extend the node form with form-level
//   submit handlers that adjust $form_state['values'] prior to those values
//   being used to update the entity. Module authors are encouraged to instead
//   adjust the node directly within a hook_node_submit() implementation. For
//   Drupal 8, evaluate whether the pattern of triggering form-level submit
//   handlers during button-level submit processing is worth supporting
//   properly, and if so, add a Form API function for doing so.
  unset($form_state['submit_handlers']);
  form_execute_handlers('submit', $form, $form_state);
  $entity_type = $form_state['entity_type'];
  // retreive the original entity.
  $entity = $form_state[$entity_type];
  // apply values form form and fields forms to $entity.
  entity_form_submit_build_entity($entity_type, $entity, $form, $form_state);
  $form_state['customizable_entities_preview'] = customizable_entities_preview($entity_type, $entity);
  $form_state['rebuild'] = TRUE;
}

// fork of the node_preview() function
function customizable_entities_preview($entity_type, $entity) {
  // Clone the entity before previewing it to prevent the entity itself from being
  // modified.
  $cloned_entity = clone $entity;
  if (entity_access('create', $entity_type, $cloned_entity) || entity_access('update', $entity_type, $cloned_entity)) {
    $entity_info = entity_get_info($entity_type);
    _field_invoke_multiple('load', 'entity', array($cloned_entity->{$entity_info['entity keys']['id']} => $cloned_entity));
    // Load the user's name when needed.
    if (in_array('uid', $entity_info['schema_fields_sql']['base table'])) {
      if (isset($cloned_entity->name)) {
        // The use of isset() is mandatory in the context of user IDs, because
        // user ID 0 denotes the anonymous user.
        if ($user = user_load_by_name($cloned_entity->name)) {
          $cloned_entity->uid = $user->uid;
          $cloned_entity->picture = $user->picture;
        }
        else {
          $cloned_entity->uid = 0; // anonymous user
        }
      }
      elseif ($cloned_entity->uid) {
        $user = user_load($cloned_entity->uid);
        $cloned_entity->name = $user->name;
        $cloned_entity->picture = $user->picture;
      }
    }

    if (in_array('changed', $entity_info['schema_fields_sql']['base table'])) {
      $cloned_entity->changed = REQUEST_TIME;
    }
    $entities = array($cloned_entity->{$entity_info['entity keys']['id']} => $cloned_entity);

    // Display a preview of the entity.
    if (!form_get_errors()) {
      $cloned_entity->in_preview = TRUE;
      $output = theme('customizable_entities_preview', array('entity' => $cloned_entity, 'entity_type' => $entity_type));
      unset($cloned_entity->in_preview);
    }
    drupal_set_title(t('Preview'), PASS_THROUGH);

    return $output;
  }
}

function customizable_entities_add_page($entity_type = NULL) {
  $item = menu_get_item();
  $content = system_admin_menu_block($item);
  // Bypass the <crud path>/add listing if only one bundle is available.
  if (count($content) == 1) {
    $item = array_shift($content);
    drupal_goto($item['href']);
  }
  return theme('customizable_entities_add_list', array('entity_type' => $entity_type, 'content' => $content));
}
