<?php
/**
 * Form constructor for the content administration form.
 */

/**
 * Implements hook_customizable_entities_operations().
 */
function customizable_entities_customizable_entities_operations($entity_type) {
  $info = entity_get_info($entity_type);
  if (!$entity_type || !$info) {
    return array();
  }
  $operations = array();
  if (in_array('status', $info['schema_fields_sql']['base table'])) {
    $operations['publish'] = array(
      'label' => t('Publish selected @label', array('@label' => $info['label'])),
      'callback' => 'customizable_entities_mass_update',
      'callback arguments' => array('updates' => array('status' => NODE_PUBLISHED)),
    );

    $operations['unpublish'] = array(
      'label' => t('Unpublish selected @label', array('@label' => $info['label'])),
      'callback' => 'customizable_entities_mass_update',
      'callback arguments' => array('updates' => array('status' => NODE_NOT_PUBLISHED)),
    );
  }

  if (in_array('promote', $info['schema_fields_sql']['base table'])) {
    $operations['promote'] = array(
      'label' => t('Promote selected @label to front page', array('@label' => $info['label'])),
      'callback' => 'customizable_entities_mass_update',
      'callback arguments' => array('updates' => array(
          'status' => NODE_PUBLISHED,
          'promote' => NODE_PROMOTED,
        )),
    );

    $operations['demote'] = array(
      'label' => t('Demote selected @label from front page', array('@label' => $info['label'])),
      'callback' => 'customizable_entities_mass_update',
      'callback arguments' => array('updates' => array('promote' => NODE_NOT_PROMOTED)),
    );
  }
  if (in_array('sticky', $info['schema_fields_sql']['base table'])) {
    $operations['sticky'] = array(
      'label' => t('Make selected @label sticky', array('@label' => $info['label'])),
      'callback' => 'customizable_entities_mass_update',
      'callback arguments' => array('updates' => array(
          'status' => NODE_PUBLISHED,
          'sticky' => NODE_STICKY,
        )),
    );

    $operations['unsticky'] = array(
      'label' => t('Make selected @label not sticky', array('@label' => $info['label'])),
      'callback' => 'customizable_entities_mass_update',
      'callback arguments' => array('updates' => array('sticky' => NODE_NOT_STICKY)),
    );
  }
  if (!empty($info['customizable entities']['crud']['delete'])) {
    $operations['delete'] = array(
      'label' => t('Delete selected @label', array('@label' => $info['label'])),
      'callback' => NULL,
    );
  }
  return $operations;
}

/**
 * List entity administration filters that can be applied.
 *
 * Inspired by the node_filters() core function.
 *
 * @param $entity_type
 *   String, the type of the entity.
 *
 * @return
 *   An associative array of filters.
 */
function customizable_entities_filters($entity_type) {
  $filters = array();
  $info = entity_get_info($entity_type);
  $has_active = in_array('status', $info['schema_fields_sql']['base table']);
  $has_promote = in_array('promote', $info['schema_fields_sql']['base table']);
  $has_sticky = in_array('sticky', $info['schema_fields_sql']['base table']);
  // Regular filters
  if ($has_active || $has_promote || $has_sticky) {
    $filters['status'] = array(
      'title' => t('status'),
      'options' => array(
        '[any]' => t('any'),
      ),
    );
    if ($has_active) {
      $filters['status']['options']['status-1'] = t('published');
      $filters['status']['options']['status-0'] = t('not published');
    }
    if ($has_promote) {
      $filters['status']['options']['promote-1'] = t('promoted');
      $filters['status']['options']['promote-0'] = t('not promoted');
    }
    if ($has_sticky) {
      $filters['status']['options']['sticky-1'] = t('sticky');
      $filters['status']['options']['sticky-0'] = t('not sticky');
    }
  }

  // Include translation states if we have this module enabled
  // @TODO
  if (module_exists('entity translation')) {
    $filters['status']['options'] += array(
      'translate-0' => t('Up to date translation'),
      'translate-1' => t('Outdated translation'),
    );
  }

  $has_bundle = (!empty($info['entity keys']['bundle']));
  if ($has_bundle) {
    $filters['type'] = array(
      'title' => t('type'),
      'options' => array(
        '[any]' => t('any'),
      ),
    );
    foreach ($info['bundles'] as $type => $datas) {
      $filters['type']['options'][$type] = $datas['label'];
    }
  }

  // Language filter if there is a list of languages
  $has_language = in_array('language', $info['schema_fields_sql']['base table']);
  if ($has_language && $languages = module_invoke('locale', 'language_list')) {
    $languages = array(LANGUAGE_NONE => t('Language neutral')) + $languages;
    $filters['language'] = array(
      'title' => t('language'),
      'options' => array(
        '[any]' => t('any'),
      ) + $languages,
    );
  }

  $additional_headers = module_invoke_all('customizable_entities_overview_page_headers', $entity_type);
  if ($additional_headers) {
    foreach ($additional_headers as $header_name => $header_datas) {
      if (isset($header_datas['field']) && strpos($header_datas['field'], '.') === FALSE && !empty($header_datas['options'])) {
        // build_list
        $list = array();
        foreach ($header_datas['options'] as $key => $title) {
          $list["{$header_datas['field']}-{$key}"] = $title;
        }
        $filters[$header_name] = array(
          'title' => drupal_strtolower($header_datas['data']),
          'options' => array(
            '[any]' => t('any'),
          ) + $list,
        );
      }
    }
  }

  return $filters;
}

/**
 * Applies filters for entity administration filters based on session.
 *
 * Inspired by the node_build_filter_query() core function.
 *
 * @param $entity_type
 *   String, the type of the entity.
 * @param $query
 *   A SelectQuery to which the filters should be applied.
 */
function customizable_entities_build_filter_query(SelectQueryInterface $query, $entity_type) {
  // Build query
  $session_name = "customizable_entities_{$entity_type}_overview_filter";
  $filter_data = isset($_SESSION[$session_name]) ? $_SESSION[$session_name] : array();
  foreach ($filter_data as $index => $filter) {
    list($key, $value) = $filter;
    switch ($key) {
      case 'status':
        // Note: no exploitable hole as $key/$value have already been checked when submitted
        list($key, $value) = explode('-', $value, 2);
      case 'type':
      case 'language':
        $query->condition('base.' . $key, $value);
        break;
      default:
        if (strpos($value, '-') !== FALSE) {
          list($key, $value) = explode('-', $value, 2);
        }
        $info = entity_get_info($entity_type);
        if (in_array($key, $info['schema_fields_sql']['base table'])) {
          $query->condition('base.' . $key, $value);
        }
    }
  }
}

/**
 * Returns the entity administration filters form array to customizable_entities_admin_entitie().
 *
 * Inspired by the node_filter_form() core function.
 *
 * @param $entity_type
 *   String, the type of the entity.
 *
 * @see customizable_entities_admin_entities()
 * @see customizable_entities_admin_entities_submit()
 * @see customizable_entities_admin_entities_validate()
 * @see customizable_entities_filter_form_submit()
 * @see customizable_entities_multiple_delete_confirm()
 * @see customizable_entities_multiple_delete_confirm_submit()
 *
 * @ingroup forms
 */
function customizable_entities_filter_form($entity_type) {
  $info = entity_get_info($entity_type);
  $form['entity_type'] = array(
    '#type' => 'value',
    '#value' => $entity_type,
  );
  $session_name = "customizable_entities_{$entity_type}_overview_filter";
  $session = isset($_SESSION[$session_name]) ? $_SESSION[$session_name] : array();
  $filters = customizable_entities_filters($entity_type);

  if (empty($filters)) {
    return array();
  }
  $i = 0;
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Show only items where'),
    '#theme' => 'exposed_filters__node',
  );
  foreach ($session as $filter) {
    list($type, $value) = $filter;
    if ($type == 'term') {
      // Load term name from DB rather than search and parse options array.
      $value = module_invoke('taxonomy', 'term_load', $value);
      $value = $value->name;
    }
    elseif ($type == 'language') {
      $value = $value == LANGUAGE_NONE ? t('Language neutral') : module_invoke('locale', 'language_name', $value);
    }
    else {
      $value = $filters[$type]['options'][$value];
    }
    $t_args = array(
      '%property' => $filters[$type]['title'],
      '%value' => $value,
    );
    if ($i++) {
      $form['filters']['current'][] = array('#markup' => t('and where %property is %value', $t_args));
    }
    else {
      $form['filters']['current'][] = array('#markup' => t('where %property is %value', $t_args));
    }
    $remove_filters = array('type', 'language');
    $additional_headers = module_invoke_all('customizable_entities_overview_page_headers', $entity_type);
    if ($additional_headers) {
      foreach ($additional_headers as $header_name => $header_datas) {
        if (isset($header_datas['field']) && strpos($header_datas['field'], '.') === FALSE && !empty($header_datas['options'])) {
          $remove_filters[] = $header_name;
        }
      }
    }
    if (in_array($type, $remove_filters)) {
      // Remove the option if it is already being filtered on.
      unset($filters[$type]);
    }
  }

  $form['filters']['status'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('clearfix')),
    '#prefix' => ($i ? '<div class="additional-filters">' . t('and where') . '</div>' : ''),
  );
  $form['filters']['status']['filters'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('filters')),
  );
  foreach ($filters as $key => $filter) {
    $form['filters']['status']['filters'][$key] = array(
      '#type' => 'select',
      '#options' => $filter['options'],
      '#title' => $filter['title'],
      '#default_value' => '[any]',
    );
  }

  $form['filters']['status']['actions'] = array(
    '#type' => 'actions',
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['filters']['status']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => count($session) ? t('Refine') : t('Filter'),
  );
  if (count($session)) {
    $form['filters']['status']['actions']['undo'] = array(
      '#type' => 'submit',
      '#value' => t('Undo'),
    );
    $form['filters']['status']['actions']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
    );
  }

  drupal_add_js('misc/form.js');

  return $form;
}

/**
 * Form submission handler for customizable_entities_filter_form().
 *
 * Inspired by the node_filter_form_submit() core function.
 *
 * @see customizable_entities_admin_content()
 * @see customizable_entities_admin_entities()
 * @see customizable_entities_admin_entities_submit()
 * @see customizable_entities_admin_entities_validate()
 * @see customizable_entities_filter_form()
 * @see customizable_entities_multiple_delete_confirm()
 * @see customizable_entities_multiple_delete_confirm_submit()
 */
function customizable_entities_filter_form_submit($form, &$form_state) {
  $entity_type = $form_state['values']['entity_type'];
  $session_name = "customizable_entities_{$entity_type}_overview_filter";
  $filters = customizable_entities_filters($entity_type);
  switch ($form_state['values']['op']) {
    case t('Filter'):
    case t('Refine'):
      // Apply every filter that has a choice selected other than 'any'.
      foreach ($filters as $filter => $options) {
        if (isset($form_state['values'][$filter]) && $form_state['values'][$filter] != '[any]') {
          // Flatten the options array to accommodate hierarchical/nested options.
          $flat_options = form_options_flatten($filters[$filter]['options']);
          // Only accept valid selections offered on the dropdown, block bad input.
          if (isset($flat_options[$form_state['values'][$filter]])) {
            $_SESSION[$session_name][] = array($filter, $form_state['values'][$filter]);
          }
        }
      }
      break;
    case t('Undo'):
      array_pop($_SESSION[$session_name]);
      break;
    case t('Reset'):
      $_SESSION[$session_name] = array();
      break;
  }
}

/**
 * Make mass update of entities, changing all entities in the $entities array
 * to update them with the field values in $updates.
 *
 * IMPORTANT NOTE: This function is intended to work when called from a form
 * submission handler. Calling it outside of the form submission process may not
 * work correctly. Inspired by the node_mass_update() core function.
 *
 * @param $entity_type
 *   String, the type of the entity.
 * @param array $entities
 *   Array of entities ids to update.
 * @param array $updates
 *   Array of key/value pairs with entity field names and the value to update
 *   that field to.
 */
function customizable_entities_mass_update($entity_type, $entities, $updates) {
  // We use batch processing to prevent timeout when updating a large number
  // of entities.
  if (count($entities) > 10) {
    $batch = array(
      'operations' => array(
        array('_customizable_entities_mass_update_batch_process', array($entity_type, $entities, $updates)),
      ),
      'finished' => '_customizable_entities_mass_update_batch_finished',
      'title' => t('Processing'),
      // We use a single multi-pass operation, so the default
      // 'Remaining x of y operations' message will be confusing here.
      'progress_message' => '',
      'error_message' => t('The update has encountered an error.'),
      // The operations do not live in the .module file, so we need to
      // tell the batch engine which file to load before calling them.
      'file' => drupal_get_path('module', 'customizable_entities') . '/pages/customizable_entities_entity_overview.pages.inc',
    );
    batch_set($batch);
  }
  else {
    foreach ($entities as $id) {
      _customizable_entities_mass_update_helper($entity_type, $id, $updates);
    }
    drupal_set_message(t('The update has been performed.'));
  }
}

/**
 * Updates individual entities when fewer than 10 are queued.
 *
 * @param $entity_type
 *   String, the type of the entity.
 * @param $id
 *   ID of entity to update.
 * @param $updates
 *   Associative array of updates.
 *
 * @return object
 *   An updated entity object.
 *
 * @see customizable_entities_mass_update()
 */
function _customizable_entities_mass_update_helper($entity_type, $id, $updates) {
  $entities = entity_load($entity_type, array($id), array(), TRUE);
  $entity = $entities[$id];
  // For efficiency manually save the original entity before applying any changes.
  $entity->original = clone $entity;
  foreach ($updates as $name => $value) {
    $entity->$name = $value;
  }
  entity_save($entity_type, $entity);
  return $entity;
}

/**
 * Executes a batch operation for customizable_entities_mass_update().
 *
 * Inspired by the node_mass_update_batch_process() core function.
 *
 * @param $entity_type
 *   String, the type of the entity.
 * @param array $entities
 *   An array of entities IDs.
 * @param array $updates
 *   Associative array of updates.
 * @param array $context
 *   An array of contextual key/values.
 */
function _customizable_entities_mass_update_batch_process($entity_type, $entities, $updates, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($entities);
    $context['sandbox']['entities'] = $entities;
  }

  // Process entities by groups of 5.
  $count = min(5, count($context['sandbox']['entities']));
  for ($i = 1; $i <= $count; $i++) {
    // For each id, load the entities, reset the values, and save it.
    $id = array_shift($context['sandbox']['entities']);
    $entity = _customizable_entities_mass_update_helper($entity_type, $id, $updates);

    // Store result for post-processing in the finished callback.
    $label = entity_label($entity_type, $entity);
    $path = current(entity_uri($entity_type, $entity));
    $context['results'][] = l($label, $path);

    // Update our progress information.
    $context['sandbox']['progress']++;
  }

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Menu callback: Reports the status of batch operation for customizable_entities_mass_update().
 *
 * Inspired by the node_mass_update_batch_finished() core function.
 *
 * @param bool $success
 *   A boolean indicating whether the batch mass update operation successfully
 *   concluded.
 * @param int $results
 *   The number of entities updated via the batch mode process.
 * @param array $operations
 *   An array of function calls (not used in this function).
 */
function _customizable_entities_mass_update_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('The update has been performed.'));
  }
  else {
    drupal_set_message(t('An error occurred and processing did not complete.'), 'error');
    $message = format_plural(count($results), '1 item successfully processed:', '@count items successfully processed:');
    $message .= theme('item_list', array('items' => $results));
    drupal_set_message($message);
  }
}

/**
 * Page callback: Form constructor for the entity administration form.
 *
 * Inspired by the node_admin_content() core function.
 *
 * @see customizable_entities_admin_entities()
 * @see customizable_entities_admin_entities_submit()
 * @see customizable_entities_admin_entities_validate()
 * @see customizable_entities_filter_form()
 * @see customizable_entities_filter_form_submit()
 * @see entity_menu()
 * @see customizable_entities_multiple_delete_confirm()
 * @see customizable_entities_multiple_delete_confirm_submit()
 * @ingroup forms
 */
function customizable_entities_admin_content($form, $form_state, $entity_type) {
  if (isset($form_state['values']['operation']) && $form_state['values']['operation'] == 'delete') {
    return customizable_entities_multiple_delete_confirm($form, $form_state, $entity_type, array_filter($form_state['values']['entities']));
  }
  $form['filter'] = customizable_entities_filter_form($entity_type);
  $form['#submit'][] = 'customizable_entities_filter_form_submit';
  $form['admin'] = customizable_entities_admin_entities($entity_type);
  $form['entity_type'] = array(
    '#type' => 'value',
    '#value' => $entity_type,
  );

  return $form;
}

/**
 * Form builder: Builds the entity administration overview.
 *
 * Inspired by the node_admin_nodes() core function.
 *
 * @param $entity_type
 *   String, the type of the entity.
 *
 * @see customizable_entities_admin_entities_submit()
 * @see customizable_entities_admin_entities_validate()
 * @see customizable_entities_filter_form()
 * @see customizable_entities_filter_form_submit()
 * @see customizable_entities_multiple_delete_confirm()
 * @see customizable_entities_multiple_delete_confirm_submit()
 *
 * @ingroup forms
 */
function customizable_entities_admin_entities($entity_type) {
  $info = entity_get_info($entity_type);
  $admin_access = user_access(customizable_entities_permission_item('administer', $entity_type));

  // Build the 'Update options' form.
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#attributes' => array('class' => array('container-inline')),
    '#access' => $admin_access,
  );
  $options = array();
  foreach (module_invoke_all('customizable_entities_operations', $entity_type) as $operation => $array) {
    $options[$operation] = $array['label'];
  }
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#title' => t('Operation'),
    '#title_display' => 'invisible',
    '#options' => $options,
    '#default_value' => 'approve',
  );
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#validate' => array('customizable_entities_admin_entities_validate'),
    '#submit' => array('customizable_entities_admin_entities_submit'),
  );

  // Build the sortable table header.
  $alias = 'base';
  $header = array(
    'title' => array(
      'data' => t('Title'),
    ),
  );
  if (!empty($info['entity keys']['label'])) {
    $header['title']['field'] = "{$alias}.{$info['entity keys']['label']}";
  }
  $has_bundle = !empty($info['entity keys']['bundle']);
  if ($has_bundle) {
    $bundle_type = customizable_entities_entity_find_bundle_entity_type($entity_type);
    $bundle_info = entity_get_info($bundle_type);
    if ($bundle_info) {
      $bundle_title = $bundle_info['label'];
    }
    $header[$info['entity keys']['bundle']] = array(
      'data' => check_plain($bundle_title),
      'field' => "{$alias}.{$info['entity keys']['bundle']}",
    );
  }
  $has_author = in_array('uid', $info['schema_fields_sql']['base table']);
  if ($has_author) {
    $header['author'] = t('Author');
  }
  $has_active = in_array('status', $info['schema_fields_sql']['base table']);
  if ($has_active) {
    $header['status'] = array(
      'data' => t('Status'),
      'field' => "{$alias}.status",
    );
  }
  $has_changed = in_array('changed', $info['schema_fields_sql']['base table']);
  if ($has_changed) {
    $header['changed'] = array(
      'data' => t('Updated'),
      'field' => "{$alias}.changed",
      'sort' => 'desc',
    );
  }

  $has_created = in_array('created', $info['schema_fields_sql']['base table']);
  if ($has_created) {
    $header['created'] = array(
      'data' => t('Created'),
      'field' => "{$alias}.created",
    );
  }

  $multilanguage = FALSE;
  $has_language = in_array('language', $info['schema_fields_sql']['base table']);
  if ($has_language) {
    // Enable language column if translation module is enabled or if we have any
    // entity with language.
    $multilanguage = (module_exists('translation') || db_query_range('SELECT 1 FROM {' . $info['base table'] . '} WHERE language <> :language', 0, 1, array(':language' => LANGUAGE_NONE))->fetchField());
    if ($multilanguage) {
      $header['language'] = array(
        'data' => t('Language'),
        'field' => "{$alias}.language",
      );
    }
  }

  $additional_headers = module_invoke_all('customizable_entities_overview_page_headers', $entity_type);
  if ($additional_headers) {
    foreach ($additional_headers as $header_name => $header_datas) {
      if (isset($header_datas['field']) && strpos($header_datas['field'], '.') !== FALSE) {
        $header_datas['field'] = "{$alias}.{$header_datas['field']}";
      }
    }
    $header += $additional_headers;
  }

  if (!empty($info['exportable'])) {
    $header['entity_status'] = array(
      'data' => t('Export status'),
    );
  }

  $header['operations'] = array('data' => t('Operations'));

  $query = db_select($info['base table'], $alias)->extend('PagerDefault')->extend('TableSort');
  customizable_entities_build_filter_query($query, $entity_type);

  $id_field = $info['entity keys']['id'];

  if (!$admin_access) { //!user_access('bypass node access')) {
    // If the user is able to view their own unpublished nodes, allow them
    // to see these in addition to published nodes. Check that they actually
    // have some unpublished nodes to view before adding the condition.
    $op_access_item = 'view unpublished';
    $where_uid =  '';
    $where_arg = array(':status' => 0);
    if ($has_author) {
      $op_access_item = 'view own unpublished';
      $where_uid = ' AND uid = :uid';
      $where_arg[':uid'] = $GLOBALS['user']->uid;
    }
    $access_item = customizable_entities_permission_item($op_access_item, $entity_type);
    if (user_access($access_item) && $own_unpublished = db_query('SELECT ' . $id_field . ' FROM {' . $info['base table'] . '} WHERE status = :status' . $where_uid, $where_arg)->fetchCol()) {
      $query->condition(db_or()
        ->condition("{$alias}.status", 1)
        ->condition("{$alias}.{$id_field}", $own_unpublished, 'IN')
      );
    }
    else {
      // If not, restrict the query to published entities.
      $query->condition("{$alias}.status", 1);
    }
  }

  $crud_path = $info['customizable entities']['crud path'];
  $ids = $query
    ->fields($alias, array($id_field))
    ->limit(50)
    ->orderByHeader($header)
    ->addTag('entity_access')
    ->execute()
    ->fetchCol();
  $entities = entity_load($entity_type, $ids);

  // Prepare the list of entities.
  $languages = language_list();
  $destination = drupal_get_destination();
  $options = array();
  foreach ($entities as $entity) {
    $id_url = (!empty($info['entity keys']['name'])) ? str_replace('_', '-', $entity->{$info['entity keys']['name']}) : $entity->$id_field;
    $langcode = entity_language($entity_type, $entity);
    $l_options = $langcode != LANGUAGE_NONE && isset($languages[$langcode]) ? array('language' => $languages[$langcode]) : array();
    $options[$entity->$id_field] = array(
      'title' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => entity_label($entity_type, $entity),
          '#href' => current(entity_uri($entity_type, $entity)),
          '#options' => $l_options,
          //'#suffix' => ' ' . theme('mark', array('type' => node_mark($node->nid, $node->changed))),
        ),
      ),
    );
    if (module_exists('entity_admin')) {
      $options[$entity->$id_field]['#suffix'] = ' ' . theme('mark', array('type' => entity_admin_mark($entity_type, $entity)));
    }
    if ($has_bundle) {
      $bundle_name = $entity->{$info['entity keys']['bundle']};
      if (!empty($info['bundles'][$bundle_name]['label'])) {
        $bundle_title = $info['bundles'][$bundle_name]['label'];
      }
      else {
        // The bundle has been deleted.
        $bundle_title = t('@bundle (deleted)', array('@bundle' => $entity->{$info['entity keys']['bundle']}));
      }
      $options[$entity->$id_field]['type'] = $bundle_title;
    }
    if ($has_author) {
      $options[$entity->$id_field]['author'] = theme('username', array('account' => user_load($entity->uid)));
    }
    if ($has_active) {
      $options[$entity->$id_field]['status'] = $entity->status ? t('published') : t('not published');
    }
    if ($has_changed) {
      $options[$entity->$id_field]['changed'] = format_date($entity->changed, 'short');
    }
    if ($has_created) {
      $options[$entity->$id_field]['created'] = format_date($entity->created, 'short');
    }
    if ($has_language) {
      if ($multilanguage) {
        if ($langcode == LANGUAGE_NONE || isset($languages[$langcode])) {
          $options[$entity->$id_field]['language'] = $langcode == LANGUAGE_NONE ? t('Language neutral') : t($languages[$langcode]->name);
        }
        else {
          $options[$entity->$id_field]['language'] = t('Undefined language (@langcode)', array('@langcode' => $langcode));
        }
      }
    }
    if (!empty($info['exportable'])) {
      $options[$entity->$id_field]['entity_status'] = theme('entity_status', array('status' => $entity->{$info['entity keys']['status']}));
    }


    if ($additional_headers) {
      $additional_col = array();
      $additional_values = module_invoke_all('customizable_entities_overview_page_values', $entity_type, $entity);
      foreach ($additional_headers as $header_name => $header_datas) {
        if (isset($additional_values[$header_name])) {
          $additional_col[$header_name] = $additional_values[$header_name];
        }
        elseif (isset($header_datas['field']) && isset($entity->{$header_datas['field']})) {
          $additional_col[$header_name] = $entity->{$header_datas['field']};
        }
        else {
          $additional_col[$header_name] = ''; // Empty cell.
        }
      }
      $options[$entity->$id_field] += $additional_col;
    }

    // Build a list of all the accessible operations for the current entity.
    $operations = array();
    $crud = $info['customizable entities']['crud'];
    // In case this is a bundle, we add links to the field ui tabs.
    $field_ui = !empty($info['bundle of']) && entity_type_is_fieldable($info['bundle of']) && module_exists('field_ui');
    // For exportable entities we add an export link.
    $exportable = !empty($info['exportable']);
    // If i18n integration is enabled, add a link to the translate tab.
    $i18n = !empty($info['i18n controller class']);

    // Add operations depending on the status.
    if (entity_has_status($entity_type, $entity, ENTITY_FIXED)) {
      $operations['clone'] = array(
        'title' => t('clone'),
        'href' => $crud_path . '/' . $id_url . '/clone',
        'query' => $destination,
      );
    }
    else {
      if (!empty($crud['update']) && entity_access('update', $entity_type, $entity)) {
        $operations['edit'] = array(
          'title' => t('edit'),
          'href' => $crud_path . '/' . $id_url . '/edit',
          'query' => $destination,
        );
      }

      if ($field_ui) {
        $admin_ui_path = $info['admin ui']['path'];
        $operations['fields'] = array(
          'title' => t('manage fields'),
          'href' => $admin_ui_path . '/manage/' . $id_url . '/fields',
          'query' => $destination,
        );
        $operations['display'] = array(
          'title' => t('manage display'),
          'href' => $admin_ui_path . '/manage/' . $id_url . '/display',
          'query' => $destination,
        );
      }
      if ($i18n) {
        $operations['translate'] = array(
          'title' => t('translate'),
          'href' => $crud_path . '/' . $id_url . '/translate',
          'query' => $destination,
        );
      }
      // Cloning a entry may be a problem for linking drupal and limesurvey...
      if ($exportable) {
        $operations['translate'] = array(
          'title' => t('clone'),
          'href' => $crud_path . '/' . $id_url . '/clone',
          'query' => $destination,
        );
      }

      if ((empty($info['exportable']) || !entity_has_status($entity_type, $entity, ENTITY_IN_CODE))
        && (!empty($crud['delete']) && entity_access('delete', $entity_type, $entity))) {
        $operations['delete'] = array(
          'title' => t('delete'),
          'href' => $crud_path . '/' . $id_url . '/delete',
          'query' => $destination,
        );
      }
      elseif (entity_has_status($entity_type, $entity, ENTITY_OVERRIDDEN)) {
        $operations['revert'] = array(
          'title' => t('revert'),
          'href' => $crud_path . '/' . $id_url . '/revert',
          'query' => $destination,
        );
      }
    }
    if ($exportable) {
      $operations['export'] = array(
        'title' => t('export'),
        'href' => $crud_path . '/' . $id_url . '/export',
        'query' => $destination,
      );
    }

    $additional_links = module_invoke_all('customizable_entities_overview_page_links', $entity_type, $entity);
    if ($additional_links) {
      foreach ($additional_links as $link_name => $link_datas) {
        $links[$link_name] = array(
          'title' => $link_datas['title'],
          'href' => $crud_path . '/' . $id_url . '/' . $link_datas['suffix_path'],
          'query' => $destination,
        );
      }
      $operations += $links;
    }

    $options[$entity->$id_field]['operations'] = array();
    if (count($operations) > 1) {
      // Render an unordered list of operations links.
      $options[$entity->$id_field]['operations'] = array(
        'data' => array(
          '#theme' => 'links__node_operations',
          '#links' => $operations,
          '#attributes' => array('class' => array('links', 'inline')),
        ),
      );
    }
    elseif (!empty($operations)) {
      // Render the first and only operation as a link.
      $link = reset($operations);
      $options[$entity->$id_field]['operations'] = array(
        'data' => array(
          '#type' => 'link',
          '#title' => $link['title'],
          '#href' => $link['href'],
          '#options' => array('query' => $link['query']),
        ),
      );
    }
  }

  // Only use a tableselect when the current user is able to perform any
  // operations.
  if ($admin_access) {
    $form['entities'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No content available.'),
    );
  }
  // Otherwise, use a simple table.
  else {
    $form['entities'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $options,
      '#empty' => t('No content available.'),
    );
  }

  $form['pager'] = array('#markup' => theme('pager'));
  return $form;
}

/**
 * Validate customizable_entities_admin_entities form submissions.
 *
 * Checks whether any entities have been selected to perform the chosen 'Update
 * option' on. Inspired by the node_admin_nodes_validate() core function.
 *
 * @see customizable_entities_admin_entities()
 * @see customizable_entities_admin_entities_submit()
 * @see customizable_entities_filter_form()
 * @see customizable_entities_filter_form_submit()
 * @see customizable_entities_multiple_delete_confirm()
 * @see customizable_entities_multiple_delete_confirm_submit()
 */
function customizable_entities_admin_entities_validate($form, &$form_state) {
  // Error if there are no items to select.
  if (!is_array($form_state['values']['entities']) || !count(array_filter($form_state['values']['entities']))) {
    form_set_error('', t('No items selected.'));
  }
}

/**
 * Process customizable_entities_admin_entities form submissions.
 *
 * Executes the chosen 'Update option' on the selected entities. Inspired by the
 * node_admin_nodes_submit() core function.
 *
 * @see customizable_entities_admin_entities()
 * @see customizable_entities_admin_entities_validate()
 * @see customizable_entities_filter_form()
 * @see customizable_entities_filter_form_submit()
 * @see customizable_entities_multiple_delete_confirm()
 * @see customizable_entities_multiple_delete_confirm_submit()
 */
function customizable_entities_admin_entities_submit($form, &$form_state) {
  $entity_type = $form_state['values']['entity_type'];
  $operations = module_invoke_all('customizable_entities_operations', $entity_type);
  $operation = $operations[$form_state['values']['operation']];
  // Filter out unchecked entities.
  $entities = array_filter($form_state['values']['entities']);
  if ($function = $operation['callback']) {
    // Add in callback arguments if present.
    if (isset($operation['callback arguments'])) {
      $args = array_merge(array($entity_type), array($entities), $operation['callback arguments']);
    }
    else {
      $args = array($entities);
    }
    call_user_func_array($function, $args);

    cache_clear_all();
  }
  else {
    // We need to rebuild the form to go to a second step. For example, to
    // show the confirmation form for the deletion of entities.
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Multiple entity deletion confirmation form for customizable_entities_admin_content().
 *
 * Inspired by the node_multiple_delete_confirm() core function.
 *
 * @see customizable_entities_admin_entities()
 * @see customizable_entities_admin_entities_submit()
 * @see customizable_entities_admin_entities_validate()
 * @see customizable_entities_filter_form()
 * @see customizable_entities_filter_form_submit()
 * @see customizable_entities_multiple_delete_confirm_submit()
 * @ingroup forms
 */
function customizable_entities_multiple_delete_confirm($form, &$form_state, $entity_type, $entities) {

  $info = entity_get_info($entity_type);
  $form['entities'] = array(
    '#prefix' => '<ul>',
    '#suffix' => '</ul>',
    '#tree' => TRUE,
  );
  // array_filter returns only elements with TRUE values
  foreach ($entities as $id => $entity) {
    if (!empty($info['entity keys']['label'])) {
      $id_field = $info['entity keys']['id'];
      $title = db_query('SELECT ' . $info['entity keys']['label'] . ' FROM {' . $info['base table'] . '} WHERE ' . $id_field . ' = :id', array(':id' => $id))->fetchField();
    }
    else {
      // Take more cpu. Load the entity
      $entity_object = entity_load_single($entity_type, $id);
      $title = entity_label($entity_type, $entity_object);
    }

    // If the entity is generated by another, prevent that deleting this entity
    // will delete destination entity and contents to.
    // If the entity is generated by another, prevent that deleting this entity
    // will delete destination entity and contents to.
    customizable_entities_warning_deletion_message($entity_type, $entity);

    $form['entities'][$id] = array(
      '#type' => 'hidden',
      '#value' => $id,
      '#prefix' => '<li>',
      '#suffix' => check_plain($title) . "</li>\n",
    );
  }
  $form['operation'] = array(
    '#type' => 'hidden',
    '#value' => 'delete',
  );
  $form['#submit'][] = 'customizable_entities_multiple_delete_confirm_submit';
  // Transfert the entity_type value to the submit function :
  $form['entity_type'] = array(
    '#type' => 'value',
    '#value' => $entity_type,
  );
  $info = entity_get_info($entity_type);
  $overview_path = $info['customizable entities']['overview path'];
  $confirm_question = format_plural(count($entities),
    'Are you sure you want to delete this item?',
    'Are you sure you want to delete these items?');
  return confirm_form($form,
    $confirm_question,
    $overview_path, t('This action cannot be undone.'),
    t('Delete'), t('Cancel'));
}

/**
 * Form submission handler for customizable_entities_multiple_delete_confirm().
 *
 * Inspired by the node_multiple_delete_confirm_submit() core function.
 *
 * @see customizable_entities_admin_entities()
 * @see customizable_entities_admin_entities_submit()
 * @see customizable_entities_admin_entities_validate()
 * @see customizable_entities_filter_form()
 * @see customizable_entities_filter_form_submit()
 * @see customizable_entities_multiple_delete_confirm()
 */
function customizable_entities_multiple_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    // Retreive the entity type from the previous form.
    $entity_type = $form['entity_type']['#value'];
    entity_delete_multiple($entity_type, array_keys($form_state['values']['entities']));
    $count = count($form_state['values']['entities']);
    watchdog('content', 'Deleted @count posts.', array('@count' => $count));
    drupal_set_message(format_plural($count, 'Deleted 1 post.', 'Deleted @count posts.'));
  }
  $info = entity_get_info($entity_type);
  $overview_path = $info['customizable entities']['overview path'];
  $form_state['redirect'] = $overview_path;
}

