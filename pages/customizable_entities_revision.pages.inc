<?php
// Revision Stuff.

/**
 * Generates an array which displays a entity detail page.
 *
 * Inspired from node_show().
 *
 * @param $entity_type
 *   The entity type of the entity to check for.
 * @param $entity
 *   Optionally an entity to check access for. If no entity is given, it will be
 *   determined whether access is allowed for all entities of the given type.
 * @param $message
 *   A flag which sets a page title relevant to the revision being viewed.
 *
 * @return
 *   A $page element suitable for use by drupal_render().
 */
function customizable_entities_entity_show($entity_type, $entity, $message = FALSE) {

  if (is_numeric($entity)) {
    $entity = entity_revision_load($entity_type, $entity);
  }

  $entity_info = entity_get_info($entity_type);
  $id_field = $entity_info['entity keys']['id'];
  $ts_field = (in_array('timestamp', $entity_info['schema_fields_sql']['revision table'])) ? 'timestamp' : FALSE;
  if ($message) {
    drupal_set_title(t('Revision of %title from %date', array(
      '%title' => entity_label($entity_type, $entity),
      '%date' => (($ts_field) ? format_date($entity->{$ts_field}) : t('unknown date')),
    )), PASS_THROUGH);
  }

  // For markup consistency with other pages, use node_view_multiple() rather than node_view().
  $entities = entity_view($entity_type, array(
    $entity->$id_field => $entity,
  ), 'full');

  // Update the history table, stating that this user viewed this entity.
  // entity_tag_new($entity);

  return $entities;
}


function customizable_entities_entity_revision_overview($entity_type, $entity) {

  drupal_set_title(t('Revisions for %title', array(
    '%title' => entity_label($entity_type, $entity),
  )), PASS_THROUGH);

  $header = array(
    t('Revision'),
    array(
      'data' => t('Operations'),
      'colspan' => 2,
    ),
  );

  $map = array(
    'view' => customizable_entities_permission_item('view revisions', $entity_type),
    'revert' => customizable_entities_permission_item('revert revisions', $entity_type),
    'delete' => customizable_entities_permission_item('delete revisions', $entity_type),
    'administer' => customizable_entities_permission_item('administer', $entity_type),
  );

  $entity_info = entity_get_info($entity_type);
  $id_field = $entity_info['entity keys']['id'];
  $vid_field = $entity_info['entity keys']['revision'];
  $result = db_query(
    'SELECT r.vid, r.log, n.vid AS current_vid, r.timestamp
      FROM {' . $entity_info['revision table'] . '} r LEFT JOIN {' . $entity_info['base table'] . '} n ON n.' . $vid_field . ' = r.' . $vid_field . '
      WHERE r.' . $id_field . ' = :' . $id_field . '
      ORDER BY r.' . $vid_field . ' DESC', array(
    ":{$id_field}" => $entity->$id_field,
  ));
  $revisions = $result->fetchAllAssoc($vid_field);

  $path = $entity_info['customizable entities']['crud path'];
  $path_ent = $path . '/' . str_replace('_', '-', $entity->$id_field);
  $rows = array();
  $revert_permission = FALSE;

  if ((user_access($map['revert']) || user_access($map['administer'])) && entity_access('update', $entity_type, $entity)) {
    $revert_permission = TRUE;
  }
  $delete_permission = FALSE;
  if ((user_access($map['delete']) || user_access($map['administer'])) && entity_access('delete', $entity_type, $entity)) {
    $delete_permission = TRUE;
  }

  $ts_field = (in_array('timestamp', $entity_info['schema_fields_sql']['revision table'])) ? 'timestamp' : FALSE;
  $log_field = (in_array('log', $entity_info['schema_fields_sql']['revision table'])) ? 'log' : FALSE;
  foreach ($revisions as $revision) {
    $row = array();
    $operations = array();

    if ($revision->current_vid > 0) {
      $row[] = array(
        'data' => t('!date', array(
          '!date' => l(format_date($revision->$ts_field, 'short'), $path_ent),
        )) . (($revision->$log_field != '') ? '<p class="revision-log">' . filter_xss($revision->$log_field) . '</p>' : ''),
        'class' => array('revision-current'),
      );
      $operations[] = array(
        'data' => drupal_placeholder(t('current revision')),
        'class' => array('revision-current'),
        'colspan' => 2,
      );
    }
    else {
      $row[] = t('!date', array(
        '!date' => l(format_date($revision->$ts_field, 'short'), "{$path_ent}/revisions/{$revision->$vid_field}/view"),
      )) . (($revision->$log_field != '') ? '<p class="revision-log">' . filter_xss($revision->$log_field) . '</p>' : '');
      if ($revert_permission) {
        $operations[] = l(t('revert'), "{$path_ent}/revisions/{$revision->$vid_field}/revert");
      }
      if ($delete_permission) {
        $operations[] = l(t('delete'), "{$path_ent}/revisions/{$revision->$vid_field}/delete");
      }
    }
    $rows[] = array_merge($row, $operations);
  }

  $build['entity_revisions_table'] = array(
    '#theme' => 'table',
    '#rows' => $rows,
    '#header' => $header,
  );

  return $build;
}

function customizable_entities_entity_revision_revert_confirm($form, $form_state, $entity_type, $entity_revision) {
  if (is_numeric($entity_revision)) {
    $entity_revision = entity_revision_load($entity_type, $entity_revision);
  }
  $form['#entity_revision'] = $entity_revision;
  $url = entity_uri($entity_type, $entity_revision);
  $url['path'] = $url['path'] . '/revisions';
  return confirm_form($form, t('Are you sure you want to revert to the @title revision from %revision-date?', array(
    '@title' => entity_label($entity_type, $entity_revision),
    '%revision-date' => format_date($entity_revision->timestamp),
  )), $url, t('Revert'), t('Cancel'));
}

function customizable_entities_entity_revision_revert_confirm_submit($form, &$form_state) {

  $entity_revision = $form['#entity_revision'];
  $entity_type = $entity_revision->entity_type;
  $entity_info = entity_get_info($entity_type);
  $base_field = $entity_info['entity keys']['id'];
  $vid_field = $entity_info['entity keys']['revision'];
  $entity_revision->revision = 1;
  $entity_revision->log = t('Copy of the revision from %date.', array(
    '%date' => format_date($entity_revision->timestamp),
  ));
  entity_revision_set_default($entity_type, $entity_revision);
  // Save the revision.
  entity_save($entity_type, $entity_revision);

  $has_bundles = (!empty($entity_info['entity keys']['bundle']));
  $bundle_label = $has_bundles ? $entity_info['bundles'][$entity_revision->{$entity_info['entity keys']['bundle']}]['label'] : $entity_info['label'];
  $title = entity_label($entity_type, $entity_revision);
  watchdog($entity_type, '@type: reverted %title revision %revision.', array(
    '@type' => $bundle_label,
    '%title' => $title,
    '%revision' => $entity_revision->$vid_field,
  ));
  drupal_set_message(t('@type %title has been reverted back to the revision from %revision-date.', array(
    '@type' => $bundle_label,
    '%title' => $title,
    '%revision-date' => format_date($entity_revision->timestamp),
  )));
  $path_ent = $entity_info['customizable entities']['crud path'] . '/' . str_replace('_', '-', $entity_revision->$base_field);
  $form_state['redirect'] = $path_ent . '/revisions';
}

function customizable_entities_entity_revision_delete_confirm($form, $form_state, $entity_type, $entity_revision) {

  if (is_numeric($entity_revision)) {
    $entity_revision = entity_revision_load($entity_type, $entity_revision);
  }
  $form['#entity_revision'] = $entity_revision;
  $url = entity_uri($entity_type, $entity_revision);
  $url['path'] = $url['path'] . '/revisions';
  return confirm_form($form, t('Are you sure you want to delete the @type revision from %revision-date?', array(
    '@type' => entity_label($entity_type, $entity_revision),
    '%revision-date' => format_date($entity_revision->timestamp),
  )), $url, t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

function customizable_entities_entity_revision_delete_confirm_submit($form, &$form_state) {

  $entity_revision = $form['#entity_revision'];
  $entity_type = $entity_revision->entity_type;
  $entity_info = entity_get_info($entity_type);
  $base_field = $entity_info['entity keys']['id'];
  $vid_field = $entity_info['entity keys']['revision'];
  $has_bundles = (!empty($entity_info['entity keys']['bundle']));
  $bundle_label = $has_bundles ? $entity_info['bundles'][$entity_revision->{$entity_info['entity keys']['bundle']}]['label'] : $entity_info['label'];
  entity_revision_delete($entity_type, $entity_revision->$vid_field);

  watchdog($entity_type, '@type: deleted %title revision %revision.', array(
    '@type' => $bundle_label,
    '%title' => entity_label($entity_type, $entity_revision),
    '%revision' => $entity_revision->$vid_field,
  ));
  drupal_set_message(t('Revision from %revision-date of @type %title has been deleted.', array(
    '%revision-date' => format_date($entity_revision->timestamp),
    '@type' => $bundle_label,
    '%title' => entity_label($entity_type, $entity_revision),
  )));
  $path_ent = $entity_info['customizable entities']['crud path'] . '/' . str_replace('_', '-', $entity_revision->$base_field);
  $form_state['redirect'] = $path_ent;
  if (db_query('SELECT COUNT(' . $vid_field . ') FROM {' . $entity_info['revision table'] . '} WHERE ' . $base_field . ' = :' . $base_field, array(
    ':' . $base_field => $entity_revision->$base_field,
  ))->fetchField() > 1) {
    $form_state['redirect'] .= '/revisions';
  }
}
