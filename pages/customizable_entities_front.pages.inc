<?php
/*
 * callback displaying entities or nodes on front page
 *
 * fork of the node_page_default() function.
 */
function customizable_entities_page_default() {
  //http://drupal.stackexchange.com/questions/58950/how-do-i-paginate-results-with-db-query
  $entities_info = entity_get_info();
  $entity_types = array();
  foreach ($entities_info as $entity_type => $info) {
    if (!empty($info['customizable entities']) && !empty($info['schema_fields_sql']['base table'])) {
      // This is an customizable_entities entity
      $has_status[$entity_type] =  (in_array('status', $info['schema_fields_sql']['base table']));
      $has_promote[$entity_type] =  (in_array('promote', $info['schema_fields_sql']['base table']));
      $has_sticky[$entity_type] =  (in_array('sticky', $info['schema_fields_sql']['base table']));
      // If the entity does not contain staus prperty assume than it can be displayed (true by default)
      if ($has_promote[$entity_type] || $has_sticky[$entity_type]) {
        $entity_types[] = $entity_type;
      }
    }
  }
  if (empty($entity_types)) {
    // There is no customizable_entities entity to display.
    return node_page_default();
  }

  $all_entities = array();
  foreach ($entity_types as $entity_type) {
    // retrieve the corresponding entities.
    $info = $entities_info[$entity_type];
    $select = db_select($info['base table'], 'n');
    $fields = array();
    $fields[] = $info['entity keys']['id'];
    if ($has_status[$entity_type]) {
      $select->condition('n.status', 1);
      //$fields[] = 'status';
    }
    if ($has_promote[$entity_type]) {
      $select->condition('n.promote', 1);
      //$fields[] = 'promote';
    }
    if ($has_sticky[$entity_type]) {
      $select->orderBy('n.sticky', 'DESC');
      $fields[] = 'sticky';
    }
    if (in_array('created', $info['schema_fields_sql']['base table'])) {
      $select->orderBy('n.created', 'DESC');
      $fields[] = 'created';
    }
    $select->fields('n', $fields)
      //->addExpression($entity_type, 'entity_type');
      ->addTag('entity_access');
    $ids = $select->execute()->fetchAllAssoc($fields[0], PDO::FETCH_ASSOC);
    foreach ($ids as $id => $datas) {
      $store = array(
        'entity_type' => $entity_type,
        'customizable_entities_id' => $id,
      ) + $datas;
      $store += array('sticky' => 0);
      $all_entities[] = $store;
    }
  }

  if (empty($all_entities)) {
    // There is no customizable_entities entity to display.
    return node_page_default();
  }

  // Load the nodes to display on front page.
  $select = db_select('node', 'n')
    ->fields('n', array('nid', 'sticky', 'created'))
    ->condition('n.promote', 1)
    ->condition('n.status', 1)
    ->orderBy('n.sticky', 'DESC')
    ->orderBy('n.created', 'DESC')
    ->extend('PagerDefault')
    ->limit(variable_get('default_nodes_main', 10))
    ->addTag('node_access');

  $nids = $select->execute()->fetchAllAssoc('nid', PDO::FETCH_ASSOC);
  foreach ($nids as $nid => $datas) {
    $store = array(
      'entity_type' => 'node',
      'customizable_entities_id' => $nid,
    ) + $datas;
    $all_entities[] = $store;
  }

  // Sort by sticky then created
  $sticky = $created = array();
  foreach ($all_entities as $key => $row) {
    $sticky[$key]  = $row['sticky'];
    $created[$key] = $row['created'];
  }
  array_multisort($sticky, SORT_DESC, $created, SORT_DESC, $all_entities);

  // Now all entities are sorted.
  // add pager.
  $per_page = variable_get('default_nodes_main', 10);
  // Initialise the pager
  $current_page = pager_default_initialize(count($all_entities), $per_page);
  // Split your list into page sized chunks
  $chunks = array_chunk($all_entities, $per_page, TRUE);
  $page = pager_find_page();
  $offset = $per_page * $page;

  $build = array();
  $i = 1;
  foreach ($chunks[$page] as $key => $datas) {
    $entity = entity_load_single($datas['entity_type'], $datas['customizable_entities_id']);
    $build[$i] = entity_view($datas['entity_type'], array($datas['customizable_entities_id'] => $entity));
    // Set weight.
    $build[$i]['#weight'] = $i;
    $i++;
  }

  // 'rss.xml' is a path, not a file, registered in node_menu().
  drupal_add_feed('rss.xml', variable_get('site_name', 'Drupal') . ' ' . t('RSS'));
  $build['pager'] = array(
    '#theme' => 'pager',
    '#weight' => 5,
  );
  drupal_set_title('');

  return $build;
}
