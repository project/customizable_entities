(function ($) {

    Drupal.behaviors.contentTypes = {
        attach: function (context) {
            // Provide the vertical tab summaries.
            $('fieldset#edit-submission', context).drupalSetSummary(function(context) {
                var vals = [];
                vals.push(Drupal.checkPlain($('#edit-title-label', context).val()) || Drupal.t('Requires a title'));
                return vals.join(', ');
            });
            $('fieldset#edit-workflow', context).drupalSetSummary(function(context) {
                var vals = [];
                $("input[name^='customizable_entities_options']:checked", context).parent().each(function() {
                    vals.push(Drupal.checkPlain($(this).text()));
                });
                if (!$('#edit-customizable-entities-options-status', context).is(':checked')) {
                    vals.unshift(Drupal.t('Not published'));
                }
                return vals.join(', ');
            });
            $('fieldset#edit-display', context).drupalSetSummary(function(context) {
                var vals = [];
                $('input:checked', context).next('label').each(function() {
                    vals.push(Drupal.checkPlain($(this).text()));
                });
                if (!$('#edit-customizable-entities-submitted', context).is(':checked')) {
                    vals.unshift(Drupal.t("Don't display post information"));
                }
                return vals.join(', ');
            });
        }
    };

})(jQuery);
